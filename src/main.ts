/* istanbul ignore file */

import { Client, Guild, Message } from 'discord.js';
import { MongoError } from 'mongodb';
import { connect, connection } from 'mongoose';
import { initCommands, Commands } from './commands';
import { dbOpen, dbError } from './db';
import { clientError, clientMessage, clientReady, clientGuildCreate, clientGuildDelete } from './events';
import { initEnv, initLog, log, version } from './utils';

function main(): void {
  initEnv('.env');
  initLog();
  initCommands();

  log.info(`[STARTUP] Starting Tildey v${version}`);
  log.debug(`[STARTUP] Default prefix: ${process.env.TLDY_PREFIX}`);
  log.debug(`[STARTUP] Enabled commands: ${Commands.array().map(({ name }) => name).join(', ')}`);

  // If the emojis guild var is left default, warn that the custom emojis won't work
  if (process.env.TLDY_EMOJIS_GUILD === 'default') {
    log.warn('[STARTUP] TLDY_EMOJIS_GUILD variable left as default, all custom emojis will be :white_large_square:');
  }

  // Connect to the Mongo database
  connect('mongodb://127.0.0.1:27017/tildey', {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true
  }).catch(async (error: MongoError) => dbError(error, client));

  // Create a new Discord client and add event handlers
  const client: Client = new Client();
  client.on('ready', () => {
    clientReady(client);
  });
  client.on('message', (msg: Message): void => {
    clientMessage(msg);
  });
  client.on('error', clientError);
  client.on('guildCreate', (guild: Guild) => clientGuildCreate(client, guild));
  client.on('guildDelete', (guild: Guild) => clientGuildDelete(client, guild));

  // Get the Mongo connection to add event handlers to them
  connection.on('open', (): void => {
    dbOpen(client);
  });
}

main();
