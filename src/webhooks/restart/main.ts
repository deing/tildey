/* istanbul ignore file */

import { Express, Request, Response } from 'express';
import { Webhook } from '../../interfaces';
import { log, restart } from '../../utils';

export function initRestartWebhook(webhooks: Express, config: Webhook): void {
  webhooks.post(config.url, (req: Request, res: Response) => {
    if (Object.hasOwnProperty.call(req.headers, 'secret-token') && req.headers['secret-token'] === config.token) {
      log.info(`[RESTARTWH] ${config.name} received restart event`);
      res.sendStatus(200);
      restart(null);
    } else {
      res.sendStatus(403);
    }
  });
}
