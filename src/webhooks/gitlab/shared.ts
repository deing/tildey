import { RichEmbed } from 'discord.js';
import fecha from 'fecha';
import { getCustomEmoji } from '../../utils';

/**
 * @function newGitlabEmbed
 * @description Shared helper to create embeds with a certain structure for
 * all GitLab Webhook requests
 * @param {string} type What type of webhook request it is
 * @param {string} color The color to give the embed
 * @param {any} body The request body for various things
 * @returns {RichEmbed} Returns a RichEmbed with the webhook structure
 */
export function newGitlabEmbed(type: string, color: string, body: any): RichEmbed {
  const footer = `${type} — ${fecha.format(new Date(), 'YYYY-MM-DD HH:mm')}`;
  const thumbnail: string = body.user_avatar;
  const title = `${getCustomEmoji('gitlab')} GitLab`;

  return new RichEmbed()
    .setColor(color)
    .setFooter(footer)
    .setThumbnail(thumbnail)
    .setTitle(title);
}

/**
 * @function getAuthorLink
 * @description Returns a link to the author's profile
 * @param {any} body The request body
 * @returns {string}
 */
export function getAuthorLink(body: any): string {
  let username: string = body.user_name;
  if (typeof body.user !== 'undefined') {
    username = body.user.username;
  }

  if (typeof body.commit !== 'undefined' && typeof body.commit.author_url !== 'undefined') {
    return body.commit.author_url;
  }

  const webUrl: string = typeof body.project === 'undefined' ? body.repository.homepage : body.project.web_url;
  return `${webUrl.slice(0, webUrl.indexOf('/', 10) + 1)}${username}`;
}
