import { RichEmbed } from 'discord.js';
import { newGitlabEmbed, getAuthorLink } from '../shared';

export function formatIssue(body: any): RichEmbed {
  const { action, iid, title, url } = body.object_attributes;
  let issueDescription: string = body.object_attributes.description === '' ?
    body.object_attributes.title :
    body.object_attributes.description;
  if (issueDescription.length > 1000) {
    issueDescription = issueDescription.slice(0, 1000) + '…';
  }

  let description = `[${body.user.name}](${getAuthorLink(body)}) `;

  if (action === 'open') {
    description += 'opened';
  } else if (action === 'close' || action === 'update') {
    description += `${action}d`;
  } else {
    return null;
  }

  description += ` issue [#${iid}](${url}).\n`;

  return newGitlabEmbed('Issue', '#bd93f9', body)
    .addField(title, issueDescription)
    .setDescription(description);
}
