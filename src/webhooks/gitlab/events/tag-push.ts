import { RichEmbed } from 'discord.js';
import { getAuthorLink, newGitlabEmbed } from '../shared';

export function formatTagPush(body: any): RichEmbed {
  const checkoutHashLink = `${body.project.web_url}/tree/${body.checkout_sha}`;
  const checkoutHashShort: string = body.checkout_sha.slice(0, 8);
  const commitCount: number = body.total_commits_count;
  const tagLink = `${body.project.web_url}${body.ref.slice(4, body.ref.length)}`;
  const tagName: string = body.ref.slice(Number(body.ref.indexOf('/', 5)) + 1, body.ref.length);

  const description: string = `[${body.user_name}](${getAuthorLink(body)}) pushed ` +
    `tag [${tagName}](${tagLink}) with ${commitCount} ` +
    `${commitCount === 1 ? 'commit' : 'commits'}.\n` +
    `Checkout hash: [${checkoutHashShort}](${checkoutHashLink})`;

  return newGitlabEmbed('Tag Push', '#50fa7b', body)
    .setDescription(description);
}
