import { RichEmbed } from 'discord.js';
import { getAuthorLink, newGitlabEmbed } from '../shared';

export function formatNoteIssue(body: any): RichEmbed {
  const comment: string = `${body.object_attributes.note.slice(0, 80)}` +
    `${body.object_attributes.note.length > 80 ? '…' : ''}`;
  const issueIid: string = body.issue.iid;
  const issueLink: string = body.object_attributes.url.slice(0, body.object_attributes.url.indexOf('#'));
  const seeMoreLink = `[See More…](${body.object_attributes.url})`;

  const description: string = `**[${body.user.name}](${getAuthorLink(body)}) ` +
    `commented on issue [#${issueIid}](${issueLink}):**\n${comment}\n${seeMoreLink}`;

  return newGitlabEmbed('Comment on Issue', '#f1fa8c', body)
    .setDescription(description);
}
