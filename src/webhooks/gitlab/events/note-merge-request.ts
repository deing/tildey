import { RichEmbed } from 'discord.js';
import { getAuthorLink, newGitlabEmbed } from '../shared';

export function formatNoteMergeRequest(body: any): RichEmbed {
  const comment: string = `${body.object_attributes.note.slice(0, 80)}` +
    `${body.object_attributes.note.length > 80 ? '…' : ''}`;
  const mergeRequestIid: string = body.merge_request.iid;
  const mergeRequestLink: string = body.object_attributes.url.slice(0, body.object_attributes.url.indexOf('#'));
  const seeMoreLink = `[See More…](${body.object_attributes.url})`;

  const description: string = `**[${body.user.name}](${getAuthorLink(body)}) ` +
    `commented on a merge request [!${mergeRequestIid}](${mergeRequestLink}):**\n${comment}\n${seeMoreLink}`;

  return newGitlabEmbed('Comment on Merge Request', '#f1fa8c', body)
    .setDescription(description);
}
