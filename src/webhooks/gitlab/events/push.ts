import { RichEmbed } from 'discord.js';
import { getAuthorLink, newGitlabEmbed } from '../shared';

export function formatPush(body: any): RichEmbed {
  const branchLink = `${body.project.web_url}/tree/${body.after}`;
  const branchName: string = body.ref.slice((body.ref as string).lastIndexOf('/') + 1, body.ref.length);
  const commitCount: number = body.total_commits_count;
  const compareLink = `${body.project.web_url}/compare/${body.before}...${body.after}`;

  let commits = '';

  for (const commit of body.commits) {
    commits += `- [\`${commit.id.slice(0, 8)}\`](${commit.url}) ` +
      `${commit.message.slice(0, 40)}${commit.message.length > 40 ? '…\n' : ''}`;
  }

  const description: string = `[${body.user_name}](${getAuthorLink(body)}) pushed ` +
    `${commitCount} ${commitCount === 1 ? 'commit' : 'commits'} to [${branchName}](${branchLink}).\n` +
    `[Compare the changes](${compareLink}) from \`${body.before.slice(0, 8)}\` to ` +
    `\`${body.after.slice(0, 8)}\`.\n${commits}`;

  return newGitlabEmbed('Push', '#8be9fd', body)
    .setDescription(description);
}
