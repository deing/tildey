/* istanbul ignore file */

import { Client, RichEmbed, TextChannel } from 'discord.js';
import { Express, Request, Response } from 'express';
import { Webhook } from '../../interfaces';
import { log } from '../../utils';
import {
  formatIssue, formatMergeRequest, formatNoteCommit, formatNoteIssue,
  formatJob, formatNoteMergeRequest, formatPipeline, formatPush,
  formatTagPush
} from './events';

export function initGitlabWebhook(webhooks: Express, client: Client, config: Webhook): void {
  // 30 second timeout between note events
  const noteTimeout = 1000 * 30;
  // Create a date as one in 2018 so it isn't limited when the bot starts
  let latestNote: Date = new Date(2018, 1, 1);
  webhooks.post(config.url, (req: Request, res: Response) => {
    // Make sure the headers include the token and that it matches ours
    if (Object.hasOwnProperty.call(req.headers, 'x-gitlab-token') && req.headers['x-gitlab-token'] === config.token) {
      log.info(`[GITLABWH] ${config.name} received ${req.body.object_kind.toLowerCase()} event`);
      let content: RichEmbed;

      try {
        switch (req.body.object_kind.toLowerCase()) {
          case 'push':
            content = formatPush(req.body);
            break;
          case 'issue':
            content = formatIssue(req.body);
            break;
          case 'merge_request':
            content = formatMergeRequest(req.body);
            break;
          case 'tag_push':
            content = formatTagPush(req.body);
            break;
          case 'pipeline':
            content = formatPipeline(req.body);
            break;
          case 'build':
            content = formatJob(req.body);
            break;
          case 'note':
            // "Notes" are comments on certain things like commits, issues and merge requests
            switch (req.body.object_attributes.noteable_type.toLowerCase()) {
              case 'commit':
                content = formatNoteCommit(req.body);
                break;
              case 'issue':
                content = formatNoteIssue(req.body);
                break;
              case 'mergerequest':
                if ((new Date()).getTime() - latestNote.getTime() < noteTimeout) {
                  // Send 429 (Too Many Requests) if the time between notes is smaller than the timeout
                  res.sendStatus(429);
                  return;
                }

                latestNote = new Date();
                content = formatNoteMergeRequest(req.body);
                break;
              // Prevent any unknown or not-yet-implemented note events from going through and return early
              default:
                // Respond with 400 (Bad Request)
                res.sendStatus(400);
                return;
            }

            break;
          // Prevent any unknown or not-yet-implemented events from going through and return early
          default:
            // Respond with 400 (Bad Request)
            res.sendStatus(400);
            return;
        }
      } catch (error) {
        // Respond with 500 (Internal Error) if something goes wrong when formatting
        res.sendStatus(500);
        log.error(`[GITLABWH] ${error}`);
        return;
      }

      if (content === null) {
        // Respond with 202 meaning it was accepted but we're not going to do anything with it
        // This can happen when we don't want specific valid triggers to be sent through
        res.sendStatus(202);
        return;
      }

      const channel: TextChannel = client.channels.get(config.channel) as TextChannel;
      // Add the webhook name to the embed title
      content.title += ` — ${config.name}`;
      channel.send(content)
        // Respond with 200 (OK) after the message is sent
        .then(() => res.sendStatus(200))
        // Respond with 500 (Internal Error) if something goes wrong with sending the message
        .catch(() => res.sendStatus(500));
    } else {
      // If it doesn't have the token or it doesn't match, respond 403 (Forbidden)
      res.sendStatus(403);
    }
  });
}
