// Can't test this due to requiring access to the Discord API for login
/* istanbul ignore file */

import { Client } from 'discord.js';
import { MongoError } from 'mongodb';
import { log } from '../../utils';
import { db } from '..';

export async function dbError(error: MongoError, client: Client): Promise<void> {
  // If the error message includes that it can't connect to MongoDB, start Tildey but with database disabled
  if (error.message.includes('Server selection timed out')) {
    db.enabled = false;
  } else {
    log.error(`[DB] ${error}`);
    return;
  }

  if (!db.enabled) {
    log.warn('[STARTUP] Database connection not established, certain commands will not work.');
    await client.login(process.env.TLDY_TOKEN);
  }
}
