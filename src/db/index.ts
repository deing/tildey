// Database events
export { dbError } from './events/error';
export { dbOpen } from './events/open';

// Database schemas
export { guildInfo as guildInfoSchema } from './schemas/guild-info';
export { karma as karmaSchema, userKarma as userKarmaSchema } from './schemas/karma';
export { lastFM as lastFMSchema } from './schemas/lastfm';
export { pingPong as pingPongSchema } from './schemas/ping-pong';
export { quote as quoteSchema } from './schemas/quote';
export { reminder as reminderSchema } from './schemas/reminder';
export { whoisOptout as whoisOptoutSchema } from './schemas/whois';

export const db = {
  enabled: false
};
