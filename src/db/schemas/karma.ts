import { Schema } from 'mongoose';

export const userKarma: Schema = new Schema({
  user: {
    type: String,
    required: true
  },
  exemplary: {
    type: Number,
    default: 0
  },
  malice: {
    type: Number,
    default: 0
  }
}, {
  versionKey: false
});

export const karma: Schema = new Schema({
  guild: {
    type: String,
    required: true
  },
  karmas: {
    type: [userKarma],
    default: []
  }
}, {
  versionKey: false
});
