import { Schema } from 'mongoose';

export const reminder = new Schema({
  user: String,
  channel: String,
  due: Date,
  text: String,
  id: Number
}, {
  versionKey: false
});
