import { Schema } from 'mongoose';

export const whoisOptout: Schema = new Schema({
  user: {
    type: String,
    required: true
  }
}, {
  versionKey: false
});
