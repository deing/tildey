import { Schema } from 'mongoose';
import { getRandomColor } from '../../utils';

const userQuote = new Schema({
  addedBy: String,
  author: String,
  content: String,
  colour: {
    type: String,
    default: getRandomColor
  },
  quoteID: Number,
  dateAdded: {
    type: Date,
    default: Date.now
  }
}, {
  versionKey: false
});

export const quote = new Schema({
  guild: String,
  quotes: {
    type: [userQuote],
    default: []
  }
}, {
  versionKey: false
});
