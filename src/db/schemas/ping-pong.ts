import { Schema } from 'mongoose';

export const pingPong: Schema = new Schema({
  user: {
    type: Object,
    required: true
  },
  date: {
    default: Date.now,
    type: Date
  }
}, {
  versionKey: false
});
