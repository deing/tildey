import { Schema } from 'mongoose';

export const lastFM: Schema = new Schema({
  // ID for the corresponding Discord user
  discord: {
    type: String,
    required: true
  },
  // Name of their LastFM account
  lastfm: {
    type: String,
    required: false
  },
  // The last time the user called ~lastfm weekly
  weeklydate: {
    type: Date,
    required: false
  }
}, {
  versionKey: false
});
