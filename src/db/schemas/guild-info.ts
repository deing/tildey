import { Schema } from 'mongoose';

export const guildInfo = new Schema({
  guild: String,
  prefix: {
    type: String,
    default: process.env.TLDY_PREFIX
  },
  disabledCommands: [{
    default: [],
    required: true,
    type: String
  }]
}, {
  versionKey: false
});
