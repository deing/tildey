export function pluralize(word: string, postfix: string, count: number): string {
  return `${count} ${word}${count === 1 ? '' : postfix}`;
}
