import { Client, TextChannel } from 'discord.js';
import { Document, Model, model } from 'mongoose';
import { reminderSchema } from '../db';
import { log } from './log';

export const latestReminder = {
  handle: 0 as any
};

/* istanbul ignore next */
export async function nextReminder(client: Client, latestHandle: number): Promise<void> {
  const reminder: Model<Document> = model('Reminder', reminderSchema);
  const reminders: Document[] = await reminder.find({}).sort({ due: 1 }).exec();

  clearTimeout(latestHandle);
  if (reminders.length === 0) {
    latestReminder.handle = 0;
    return Promise.resolve();
  }

  const timeout: number = reminders[0].get('due') - Date.now();
  log.debug(`[NEXTREMINDER] Found ${reminders.length} reminders, latest one will fire in ${timeout / 1000}s`);
  let handle: any = 0;
  if (timeout > 60 * 60 * 1000) {
    handle = setTimeout(() => {
      nextReminder(client, latestHandle);
    }, 60 * 60 * 1000);
  } else {
    handle = setTimeout(() => {
      executeReminder(client, reminders[0].get('channel'), reminders[0].get('user'), reminders[0].get('text'));
    }, timeout);
  }

  latestReminder.handle = handle;
  return Promise.resolve();
}

/* istanbul ignore next */
async function executeReminder(client: Client, channelID: string, targetUserID: string, text: string): Promise<void> {
  const channel: TextChannel = client.channels.get(channelID) as TextChannel;
  if (channel) {
    channel.send(`<@${targetUserID}>!~ ${text}`);
  } else {
    try {
      (await client.users.get(targetUserID).createDM())
        .sendMessage(`<@${targetUserID}>!~ ${text}`);
    } catch {
      // If it can't DM them they've either deleted their account or blocked the bot
    }
  }

  const reminder: Model<Document> = model('Reminder', reminderSchema);
  const reminders: Document[] = await reminder.find({}).sort({ due: 1 }).exec();
  if (reminders.length === 0) {
    latestReminder.handle = 0;
    return Promise.resolve();
  }

  await reminders[0].remove();
  await nextReminder(client, latestReminder.handle);
  return Promise.resolve();
}
