/* istanbul ignore file */

import child from 'child_process';
import { ensureDirSync, writeJSONSync } from 'fs-extra';
import { RestartMessage } from '../interfaces';
import { log } from './log';

export function restart(msg: RestartMessage): void {
  ensureDirSync('temp');
  if (msg !== null) {
    writeJSONSync('temp/restart_message.json', msg);
  }

  if (process.env.TLDY_ENV === 'production') {
    const newTildey: child.ChildProcess = child.spawn('sh', ['-c', 'git pull && yarn && yarn production']);
    newTildey.on('error', (err: Error) => log.error(`[SUPER] ${err}`));
  } else if (process.env.TLDY_ENV === 'development') {
    log.debug('[SUPER] Restart executed in dev (Nodemon will restart Tildey)');
    if (msg === null) {
      writeJSONSync('temp/restart.json', { text: 'Restart' });
    }
  }
}
