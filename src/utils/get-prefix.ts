import { Message } from 'discord.js';
import { Prefix } from '../interfaces';
import { prefixes } from './prefixes';

/**
 * @function getPrefix
 * @description Returns the bot's prefix
 * @param message The message object with a possible Guild
 */
export function getPrefix(msg: Message): string {
  if (msg.guild) {
    const prefix: Prefix = prefixes.find((val: Prefix) => msg.guild.id === val.guild);
    if (typeof prefix !== 'undefined') {
      return prefix.prefix;
    }
  }

  return process.env.TLDY_PREFIX;
}
