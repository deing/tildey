import { Client } from 'discord.js';
import { Document, Model, model } from 'mongoose';
import { db, guildInfoSchema } from '../db';
import { Prefix } from '../interfaces';

export const prefixes: Prefix[] = [];

/* istanbul ignore next */
export async function initPrefixes(client: Client): Promise<void> {
  if (db.enabled) {
    const guildInfos: Model<Document> = model('Guild Info', guildInfoSchema);
    for (const guild of client.guilds.array()) {
      const guildInfo: Document = await guildInfos.findOne({ guild: guild.id });
      if (guildInfo) {
        prefixes.push({
          guild: guild.id,
          prefix: guildInfo.get('prefix') || process.env.TLDY_PREFIX
        });
      } else {
        prefixes.push({
          guild: guild.id,
          prefix: process.env.TLDY_PREFIX
        });
      }
    }
  } else {
    for (const guild of client.guilds.array()) {
      prefixes.push({
        guild: guild.id,
        prefix: process.env.TLDY_PREFIX
      });
    }
  }

  return Promise.resolve();
}
