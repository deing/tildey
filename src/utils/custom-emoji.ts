import { Emoji, Collection } from 'discord.js';

// Create a Collection so we don't have to do a lookup for the emojis each time
export const CustomEmojis: Collection<string, Emoji> = new Collection();

/**
 * @function getCustomEmoji
 * @description Finds the specified emoji from the CustomEmojis collection
 * @param emojiName Name of the emoji you want to get
 * @returns {Emoji|string} The Emoji or :white_large_square:
 */
export function getCustomEmoji(name: string): (Emoji|string) {
  // If the Collection doesn't have the emoji we're looking for, return white large square
  if (!CustomEmojis.has(name)) {
    return ':white_large_square:';
  }

  return CustomEmojis.get(name);
}
