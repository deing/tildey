// Due to using fetchMessages() this file can't be unit tested as it relies on the Discord API
/* istanbul ignore file */

import { Message, User } from 'discord.js';
import { getUser } from './get-user';

/**
 * @function getLastMessage
 * @description Returns the Message object of the last message of a specified user in a message's channel
 * @param msg The message to operate on
 * @param userID the ID of the User, can be in the form of a ping
 * @returns {Promise<Message>} Returns a Promise that will resolve to a Message or null if none could be found
 */
export async function getLastMessage(msg: Message, userID: string): Promise<Message> {
  const targetUser: User = getUser(msg, userID, true);
  if (targetUser === null) {
    return null;
  }

  const msgs = await msg.channel.fetchMessages();
  return msgs.filter((_msg) => _msg.author.id === targetUser.id).first();
}
