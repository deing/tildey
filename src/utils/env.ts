import { resolve } from 'path';
import joi from '@hapi/joi';
import * as dotenv from 'dotenv';
import { pathExistsSync, readFileSync } from 'fs-extra';

/**
 * @function initEnv
 * @description Initializes the .env process and checks to make sure all required
 * variables are set
 * @param {string} envFile The .env file to init
 * @returns {void} Nothing
 */
export function initEnv(envFile: string): void {
  const envPath: string = resolve(envFile);

  // Check if `.env` exists and error properly if it doesn't
  if (!pathExistsSync(envPath)) {
    throw new Error(`\`${envFile}\` does not exist, please create one using \`.env.example\``);
  }

  const configSchema: joi.ObjectSchema = joi.object({
    TLDY_PREFIX: joi.string().required(),
    TLDY_TOKEN: joi.string().required(),
    TLDY_ENV: joi.string().required(),
    TLDY_EMOJIS_GUILD: joi.string().default('default'),
    TLDY_FEEDBACK_CHANNEL: joi.string().required(),
    TLDY_SUPERS: joi.string().required(),
    TLDY_WA_TOKEN: joi.string().optional(),
    TLDY_WEBHOOKS_PORT: joi.string().optional(),
    TLDY_LASTFM_KEY: joi.string().optional(),
    TLDY_LASTFM_AGENT: joi.string().optional(),
    TLDY_PUSHOVER_KEY: joi.string().default(''),
    TLDY_PUSHOVER_USER: joi.string().default('')
  });

  // Parse the .env and validate it
  const envs = dotenv.parse(readFileSync(envPath));
  const { error } = configSchema.validate(envs);
  if (error) {
    throw error;
  }

  // Make sure the prefix is set to something other than the example
  if (envs.TLDY_PREFIX === 'prefix') {
    throw new Error('TLDY_PREFIX should probably not be "prefix"');
  }

  // Make sure the token is set to something other than the example, as that's not a valid token
  if (envs.TLDY_TOKEN === 'token') {
    throw new Error('TLDY_TOKEN has to be a Discord bot token https://discordapp.com/developers/docs/intro#bots-and-apps');
  }

  // And once everything is good, assign them in env
  dotenv.config({ path: envPath });
}
