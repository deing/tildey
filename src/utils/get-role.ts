import { Message, Role } from 'discord.js';

/**
 * @function getRole
 * @description Returns a Role object of a specified Role ID or mention
 * @param msg The message to operate on
 * @param roleID The ID of the Role, can include mention characters
 * @param silent Whether to send a message saying it didn't find any roles
 * @returns {Role} The role that's specified
 */
export function getRole(msg: Message, roleID: string, silent?: boolean): Role {
  const formattedRoleID: string = roleID.replace(/[<@&>]/g, '');
  if (!msg.guild.roles.get(formattedRoleID)) {
    // Ignore the replying part since we can't mock this in testing
    /* istanbul ignore next */
    if (!silent) {
      msg.reply(' There\'s no roles by that name in this server!~ :shrug:');
    }

    return null;
  }

  return msg.guild.roles.get(formattedRoleID);
}
