import { Command, Commands } from '../commands';

/**
 * @function getCommandsFromCategory
 * @description Returns a Command[] with all the commands from a specified category
 * @param category The category you want to find commands from
 * @returns {Command[]} Array of Commands with specified category
 */
export function getCommandsFromCategory(category: string): Command[] {
  return Commands.array().filter((command) => command.category === category);
}
