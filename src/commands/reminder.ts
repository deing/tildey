/* istanbul ignore file */

import { Message, RichEmbed, User } from 'discord.js';
import { Document, Model, model } from 'mongoose';
import { reminderSchema } from '../db';
import { getCustomEmoji, getUser, parseFuzzyTimeOffset, latestReminder, nextReminder } from '../utils';

export async function reminderCmd(msg: Message, name: string, args: string[]): Promise<void> {
  const reminder: Model<Document> = model('Reminder', reminderSchema);
  const subcommand: string = args[0] === undefined ? null : args[0].toLowerCase();
  if (subcommand === null) {
    await msg.reply(`you forgot to include arguments!~ ${getCustomEmoji('pwease')}`);
  } else if (subcommand === 'add') {
    if (args.length < 3) {
      await msg.reply(`you need to include at least a target user, duration, and message!~ ${getCustomEmoji('pwease')}`);
      return Promise.resolve();
    }

    let targetUserID = '';
    const targetUserString: string = args[1].toLowerCase();
    if (targetUserString === 'me') {
      targetUserID = msg.author.id;
    } else {
      const user: User = getUser(msg, targetUserString);
      if (user === null) {
        return Promise.resolve();
      }

      targetUserID = user.id;
    }

    let reminderOffset: number = parseFuzzyTimeOffset(args[2], 420);
    let parseError = false;
    if (reminderOffset <= 0) {
      const targetDate = new Date(args[2]);
      if (isNaN(targetDate.valueOf())) {
        parseError = true;
      } else {
        reminderOffset = targetDate.valueOf() - Date.now();
        if (reminderOffset < 0 || reminderOffset > 86400 * 1000 * 420) {
          // If in the past or over 420 days in the future
          parseError = true;
        }
      }

      if (parseError) {
        await msg.reply(`I didn't find any valid times in your message (the maximum is 420 days)!~ ${getCustomEmoji('pwease')}`);
        return Promise.resolve();
      }
    }

    const reminderMessage: string = args.slice(3).join(' ');
    const lastReminderByID: Document = (await reminder.find({}, {}, { sort: { id: -1 } }).exec())[0];
    await reminder.create({
      user: targetUserID,
      channel: msg.channel.id,
      due: new Date(Date.now() + reminderOffset),
      text: reminderMessage,
      id: lastReminderByID === undefined ? 0 : lastReminderByID.get('id') as number + 1
    });
    await msg.react(getCustomEmoji('tildey'));
  } else if (subcommand === 'list') {
    const reminders: Document[] = await reminder.find({ user: msg.author.id });
    const embed = new RichEmbed()
      .setTitle(`Reminders for ${msg.author.tag}`)
      .setColor('#bd93f9');
    embed.description = '';
    for (const reminder of reminders) {
      const due: Date = reminder.get('due');
      const id: number = reminder.get('id');
      const text: string = reminder.get('text');
      // Get the difference between the due date and now in milliseconds
      // Divide the difference by (1000 / 60 / 60) to convert it to hours
      embed.description += `\`#${id}\` ${text.length > 40 ? `${text.slice(0, 30)}…` : text} (${((due.valueOf() - Date.now()) / 1000 / 60 / 60).toFixed(2)}h left)\n`;
    }

    if (embed.description.length === 0) {
      embed.description = '**You have no pending reminders!~**';
    }

    await msg.channel.send(embed);
  } else if (subcommand === 'remove') {
    if (args.length === 0 || isNaN(parseInt(args[0], 10))) {
      await msg.reply(`you forgot to include a reminder ID to delete!~ ${getCustomEmoji('pwease')}`);
      return;
    }

    const result = await reminder.deleteOne({ user: msg.author.id, id: args[0] });
    if (result === 1) {
      await msg.react(getCustomEmoji('tildey'));
    } else {
      await msg.reply(`that doesn't seem to be one of your reminders!~ ${getCustomEmoji('pwease')}`);
    }
  } else if (subcommand === 'purge') {
    await reminder.deleteMany({ user: msg.author.id });
    await msg.react(getCustomEmoji('tildey'));
  } else {
    await msg.reply(`that's not how you use the quote command!~ ${getCustomEmoji('pwease')}`);
  }

  await nextReminder(msg.client, latestReminder.handle);
  return Promise.resolve();
}
