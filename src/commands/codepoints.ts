/* istanbul ignore file */

import { Message } from 'discord.js';
import unidata, { UnicodeCharacter, UnicodeData } from 'mjtb-unidata';

export async function codepointsCmd(msg: Message, name: string, args: string[]): Promise<void> {
  const unicode: UnicodeData = await unidata.instance().promise();
  let message = '```\n';
  for (const char of args.join(' ')) {
    const charData: UnicodeCharacter = unicode.get(char.codePointAt(0));
    message += `${char} | \\U${char.codePointAt(0).toString(16).padStart(4, '0').toUpperCase()} | ${charData.name} \n`;
  }

  if (message.length >= 2000) {
    const lines: string[] = message.slice(0, 2000).split('\n');
    lines.pop();
    message = lines.join('\n');
  }

  message += '```';
  await msg.channel.send(message);
}
