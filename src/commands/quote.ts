/* istanbul ignore file */

import { Message, RichEmbed } from 'discord.js';
import fecha from 'fecha';
import { Document, Model, model } from 'mongoose';
import { quoteSchema } from '../db';
import { getCustomEmoji, getPrefix, pluralize, getRandomColor } from '../utils';

export async function quoteCmd(msg: Message, name: string, args: string[]): Promise<void> {
  const quotes: Model<Document> = model('Quote', quoteSchema);
  if (args.length === 0) {
    const quote: Document = await quotes.findOne({ guild: msg.guild.id });
    if (quote) {
      const quotesArray: any[] = quote.get('quotes');
      const chosenQuote: any = quotesArray[Math.floor(Math.random() * quotesArray.length)];
      await msg.channel.send(renderQuote(msg, chosenQuote));
    } else {
      await msg.reply('there are no quotes from this server yet!~ :shrug:');
    }
  } else if (args[0].toLowerCase() === 'count') {
    const quote: Document = await quotes.findOne({ guild: msg.guild.id });
    if (quote) {
      await msg.reply(`there are currently \`${quote.get('quotes').length}\` quotes!~ ${getCustomEmoji('tildey')}`);
    } else {
      await msg.reply('there are no quotes from this server yet!~ :shrug:');
    }
  } else if (args.length >= 3 && args[0].toLowerCase() === 'add') {
    const quote: Document = await quotes.findOneAndUpdate({ guild: msg.guild.id }, {}, { upsert: true, new: true });
    const quotesArray = quote.get('quotes');
    quotesArray.push({
      addedBy: msg.author.id,
      author: args[1],
      content: args.splice(2, args.length).join(' '),
      colour: getRandomColor(),
      quoteID: quotesArray.length
    });

    await quote.save();
    await msg.channel.send(renderQuote(msg, quotesArray[quotesArray.length - 1]).setFooter(`Added with ID ${quotesArray.length - 1}  | Colour ${quotesArray[quotesArray.length - 1].colour}`));
  } else if (args[0].toLowerCase() === 'list' && !isNaN(parseInt(args[1], 10))) {
    const quote: Document = await quotes.findOne({ guild: msg.guild.id });
    if (quote) {
      const quotesArray: any[] = quote.get('quotes');

      if (Number(args[1]) >= quotesArray.length) {
        await msg.reply(`I couldn't find any quotes with that ID or above!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      const superOwners: string[] = process.env.TLDY_SUPERS.split(',');
      const embed: RichEmbed = new RichEmbed().setColor('#f1fa8c');
      for (let i = Number(args[1]); i < Number(args[1]) + 5; i++) {
        if (i >= quotesArray.length) {
          break;
        }

        let quoteContent: string = quotesArray[i].content;
        if (quoteContent.length >= 500) {
          quoteContent = quoteContent.slice(0, 499) + '[…]';
        }

        embed.setTitle(`Quotes from ID ${Number(args[1])} to ID ${Number(args[1]) + embed.fields.length}`);
        embed.addField(
          `ID ${quotesArray[i].quoteID}`,
          `**“${quoteContent}”**\n— ${quotesArray[i].author} ` +
          `${fecha.format(new Date(quotesArray[i].dateAdded), 'YYYY-MM-DD HH:mm:ss')}` +
          `${superOwners.includes(msg.author.id) ? ` (Added by <@${quotesArray[i].get('addedBy')}>)` : ''}` +
          ` | Colour ${quotesArray[i].colour}`
        );
      }

      await msg.channel.send(embed);
    } else {
      await msg.reply('there are no quotes from this server yet!~ :shrug:');
    }
  } else if ((args.length === 2 || args.length === 3) && args[0].toLowerCase() === 'list') {
    const quote: Document = await quotes.findOne({ guild: msg.guild.id });
    if (quote) {
      let quotesArray: any[] = quote.get('quotes').filter((quote: Document) => {
        if (/<@!?\d+>/.exec(args[1])) {
          return quote.get('author').replace(/\D/g, '') === args[1].replace(/\D/g, '');
        }

        return quote.get('author') === args[1];
      });

      const actualAmountOfQuotes: number = quotesArray.length;
      if (args.length === 3 && parseInt(args[2], 10)) {
        const page: number = parseInt(args[2], 10);
        if (quotesArray.length < page * 5) {
          await msg.reply(`there aren't that many quotes from that user!~ ${getCustomEmoji('pwease')}`);
          return;
        }

        if (page < 0) {
          await msg.reply(`you can't specify a negative page!~ ${getCustomEmoji('pwease')}`);
          return;
        }

        quotesArray = quotesArray.slice(page * 5, (page + 1) * 5);
      } else {
        quotesArray = quotesArray.slice(0, 5);
      }

      const superOwners: string[] = process.env.TLDY_SUPERS.split(',');
      const embed: RichEmbed = new RichEmbed().setColor('#ffb86c');
      for (const individualQuote of quotesArray) {
        let quoteContent: string = individualQuote.content;
        if (quoteContent.length >= 500) {
          quoteContent = quoteContent.slice(0, 499) + '[…]';
        }

        embed.addField(
          `ID ${individualQuote.quoteID}`,
          `**“${quoteContent}”**\n— ${individualQuote.author} ` +
          `${fecha.format(new Date(individualQuote.dateAdded), 'YYYY-MM-DD HH:mm:ss')}` +
          `${superOwners.includes(msg.author.id) ? ` (Added by <@${individualQuote.get('addedBy')}>)` : ''}` +
          ` | Colour ${individualQuote.colour}`
        );
      }

      embed.setTitle(`This user has ${pluralize('quote', 's', actualAmountOfQuotes)}!~`);
      if (actualAmountOfQuotes > 5) {
        embed.setFooter(`Use ${getPrefix(msg)}quote list @User 0…${Math.floor(actualAmountOfQuotes / 5)} to access different pages.`);
      }

      await msg.channel.send(embed);
    } else {
      await msg.reply('there are no quotes from this server yet!~ :shrug:');
    }
  } else if (args.length === 1 && !isNaN(parseInt(args[0], 10))) {
    const quote: Document = await quotes.findOne({ guild: msg.guild.id });
    if (quote) {
      const quotesArray: any[] = quote.get('quotes');

      if (quotesArray[Number(args[0])]) {
        await msg.channel.send(renderQuote(msg, quotesArray[Number(args[0])]));
      } else {
        await msg.reply(`I couldn't find a quote with that ID in this server!~ ${getCustomEmoji('pwease')}`);
      }
    } else {
      await msg.reply('there are no quotes from this server yet!~ :shrug:');
    }
  } else {
    await msg.reply(`that's not how you use the quote command!~ ${getCustomEmoji('pwease')}`);
  }

  return Promise.resolve();
}

function renderQuote(msg: Message, document: Document): RichEmbed {
  const superOwners: string[] = process.env.TLDY_SUPERS.split(',');
  const content = `> ${document.get('content').replace(/\n/g, '\n> ')}`;
  const embed = new RichEmbed()
    .setColor(document.get('colour'))
    .setDescription(
      `${content}\n — ${document.get('author')}` +
      `${superOwners.includes(msg.author.id) ? ` (Added by <@${document.get('addedBy')}>)` : ''}`
    )
    .setFooter(`Added ${fecha.format(new Date(document.get('dateAdded')), 'YYYY-MM-DD HH:mm:ss')} | ID ${document.get('quoteID')} | Colour ${document.get('colour')}`);
  return embed;
}
