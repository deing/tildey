/* istanbul ignore file */

import { Message } from 'discord.js';
import { getCustomEmoji } from '../utils';

export async function spongeCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length > 0) {
    const spongified: string = args.join(' ').toLowerCase().split('').map(
      (x) => Math.random() >= 0.5 ? x : x.toUpperCase()
    ).join('');
    await msg.channel.send(spongified);
  } else {
    await msg.reply(`yOU FOrgOt to INcluDe A tEXt!~ ${getCustomEmoji('pwease')}`);
  }
}
