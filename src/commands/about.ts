/* istanbul ignore file */

import { Message, RichEmbed } from 'discord.js';
import simpleGit from 'simple-git/promise';
import { version, getCustomEmoji } from '../utils';

export async function aboutCmd(msg: Message): Promise<void> {
  const { latest } = await simpleGit().log(['-1']);
  const repo = 'https://gitlab.com/tildey/tildey';
  const commitLink = `${repo}/tree/${latest.hash}`;
  const tagLink = `${repo}/tags/v${version}`;

  const versionField = `The current running version is [v${version}](${tagLink}) ` +
    `on commit [${latest.hash.slice(0, 8)}](${commitLink}).`;
  const sourceField = `Tildey is open-source and licensed under AGPL-3.0-or-later.\nAvailable on GitLab: ${repo}`;
  const developmentField = 'Want to ask questions, give suggestions ' +
    'or test the latest version?\nYou\'re welcome in the ' +
    'development server: https://discord.gg/yn8E3m4';
  const embed: RichEmbed = new RichEmbed()
    .setTitle(`${getCustomEmoji('tildey')} About Tildey!~`)
    .setColor('f1fa8c')
    .addField('Description', 'A Discord bot, because why not. https://tildey.xyz')
    .addField('Version', versionField)
    .addField('Open Source', sourceField)
    .addField('Development', developmentField);

  await msg.channel.send(embed);
}
