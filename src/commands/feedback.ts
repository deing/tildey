/* istanbul ignore file */

import { Message, TextChannel } from 'discord.js';
import { getCustomEmoji } from '../utils';

export async function feedbackCmd(msg: Message, name: string, args: string[]): Promise<void> {
  const initChannel: TextChannel = msg.channel as TextChannel;
  const feedbackChannel: TextChannel = msg.client.channels.get(process.env.TLDY_FEEDBACK_CHANNEL) as TextChannel;
  let feedbackMsg = `${msg.author.tag} in a private message: ${args.join(' ')}`;

  if (msg.guild) {
    feedbackMsg = `${msg.author.tag} in "${msg.guild.name}#${initChannel.name}": ${args.join(' ')}`;
  }

  // Sanitize user mentions, role mentions, @everyone and @here
  feedbackMsg = feedbackMsg.replace(/<@/g, '\\<\\@');
  feedbackMsg = feedbackMsg.replace(/<&/g, '\\<\\&');
  feedbackMsg = feedbackMsg.replace(/@everyone/g, '@ everyone');
  feedbackMsg = feedbackMsg.replace(/@here/g, '@ here');

  await feedbackChannel.send(feedbackMsg);
  await msg.reply(`your message was sent to the development Discord server!~ ${getCustomEmoji('tildey')}`);
}
