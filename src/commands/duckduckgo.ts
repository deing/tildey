/* istanbul ignore file */

import { Message, RichEmbed } from 'discord.js';
import fecha from 'fecha';
import he from 'he';
import { search, Result } from 'node-ddg';
import { getCustomEmoji } from '../utils';

export async function duckduckgoCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length === 0) {
    await msg.reply(`${getCustomEmoji('pwease')} You didn't include anything to search for!~`);
    return;
  }

  msg.channel.startTyping();
  try {
    const results: Result[] = await search({ query: args.join(' ') });
    if (results[0] !== null) {
      const embed: RichEmbed = new RichEmbed()
        .setTitle(`${getCustomEmoji('ddg')} DuckDuckGo`)
        .setColor('#df4e26')
        .addField('Title', he.decode(results[0].title))
        .addField('Description', he.decode(results[0].body))
        .addField('Link', `[${results[0].url}](${results[0].url})`)
        .setFooter(`Search — ${fecha.format(new Date(), 'YYYY-MM-DD HH:mm')}`);
      await msg.channel.send(embed);
    }
  } catch {
    await msg.reply(`I didn't find any results matching that query!~ ${getCustomEmoji('pwease')}`);
  }

  msg.channel.stopTyping();
}
