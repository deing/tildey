/* istanbul ignore file */

import { Message } from 'discord.js';
// @ts-ignore a "no declarations" error
import owo from 'owofy';

const owoFaces: string[] = [
  '(*^ω^)',
  '(◕‿◕✿)',
  '(◕ᴥ◕)',
  'ʕ•ᴥ•ʔ',
  'ʕ￫ᴥ￩ʔ',
  '(*^.^*)',
  'owo',
  '(｡♥‿♥｡)',
  'uwu',
  '(*￣з￣)',
  '>w<',
  '^w^',
  '(つ✧ω✧)つ',
  '(/ =ω=)/'
];

export async function owoCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length === 0) {
    await msg.reply(`you didn't include a message to ${name}fy!~ :shrug:`);
    return;
  }

  let newMessage: string = args.join(' ');
  for (let i = 1; i < (newMessage.length / 40) + 1; i++) {
    const chosenFace: string = owoFaces[Math.floor((Math.random() * owoFaces.length))];
    const facePosition: number = newMessage.indexOf(' ', i * 40);
    if (facePosition <= 0) {
      break;
    }

    newMessage = `${newMessage.slice(0, facePosition)} ${chosenFace}${newMessage.slice(facePosition, newMessage.length)}`;
  }

  newMessage = owo(newMessage).replace(/\*/g, '\\*');
  if (newMessage.length >= 2000) {
    newMessage = newMessage.slice(0, 1990) + '…';
  }

  await msg.channel.send(newMessage);
}
