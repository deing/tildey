/* istanbul ignore file */

import { Message, Role, GuildMember } from 'discord.js';
import { getCustomEmoji, getGuildMember, getRandomColor, pluralize, getPrefix } from '../utils';

export async function roleCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length === 0) {
    await msg.reply(`you forgot to include any arguments in your message!~ ${getCustomEmoji('tildey')}`);
    return;
  }

  // Iterate through author's roles and see if any of them end with a tilde
  let selectedRole: Role = null;
  for (const role of getGuildMember(msg, msg.author.id).roles.array()) {
    if (role.name.endsWith('~')) {
      selectedRole = role;
      break;
    }
  }

  // If the first argument is 'color' or 'colour', we need to change the role color
  if (/^colou?r$/.exec(args[0])) {
    // If the second argument isn't a 6 character hex color it won't work, Discord doesn't like 3 char hex
    let colour: string = args[1] ? args[1] : '';
    if (!colour.startsWith('#')) {
      colour = `#${colour}`;
    }

    if (!colour.match(/^#[A-Fa-f0-9]{6}$/g)) {
      await msg.reply(`you didn't pass a valid 6 character hex color, ie: #123456!~ ${getCustomEmoji('pwease')}`);
      return;
    }

    // If the role is still null we need to create one and afterwards add it to the user
    if (selectedRole === null) {
      const role: Role = await msg.guild.createRole({
        name: `${msg.author.username}~`,
        color: args[1],
        mentionable: true
      });
      await getGuildMember(msg, msg.author.id).addRole(role);
    } else {
      await selectedRole.setColor(args[1]);
    }
  // Else if the first argument is 'title', we need to change the role title
  } else if (args[0].toLowerCase() === 'title') {
    // Gets all the arguments past the first one and makes that the title, ie: '~role title <this is the title>'
    // Also removes tildes and trims whitespace
    const title: string = args.splice(1, args.length).join(' ').replace(/~/g, ' ').trim();
    if (selectedRole === null) {
      const role: Role = await msg.guild.createRole({
        name: `${title}~`,
        color: getRandomColor(),
        mentionable: true
      });
      await getGuildMember(msg, msg.author.id).addRole(role);
    } else {
      await selectedRole.setName(`${title}~`);
    }
  // Else if the first argument is 'delete', we wanna check if the role exists and delete it
  } else if (args[0].toLowerCase() === 'delete') {
    if (selectedRole !== null) {
      await selectedRole.delete();
    }
  } else if (args[0].toLowerCase() === 'cleanup') {
    if (!msg.member.hasPermission('MANAGE_ROLES')) {
      await msg.reply('you don\'t have the permissions to run this command!~ :no_entry_sign:');
      return;
    }

    const toDelete: Role[] = [];
    const roles: Role[] = msg.guild.roles.array();
    for (const role of roles) {
      const roleMembers: GuildMember[] = role.members.array();
      if (role.editable && roleMembers.length === 0 && role.name.endsWith('~')) {
        toDelete.push(role);
      }
    }

    if (toDelete.length === 0) {
      await msg.reply(`there are no roles to clean up at this time!~ ${getCustomEmoji('tildey')}`);
      return;
    }

    if (args.length === 2 && args[1].toLowerCase() === 'confirm') {
      const promises: Array<Promise<Role>> = [];
      for (const role of toDelete) {
        promises.push(role.delete(`Automated role cleanup executed by ${msg.author.id}`));
      }

      await Promise.all(promises);
      await msg.reply(`successfully deleted ${pluralize('role', 's', toDelete.length)}!~ ${getCustomEmoji('tildey')}`);
      return;
    }

    let deleteMessage = `Found ${pluralize('role', 's', toDelete.length)} that can be removed, `;
    deleteMessage += `use command \`${getPrefix(msg)}role cleanup confirm\` to delete them:\n`;
    for (const role of toDelete) {
      deleteMessage += `\`${role.name}\`, `;
    }

    deleteMessage = deleteMessage.slice(0, deleteMessage.length - 2) + '.';
    await msg.channel.send(deleteMessage);
    return;
  } else {
    await msg.reply(`\`${args[0]}\` isn't a valid argument!~ ${getCustomEmoji('pwease')}`);
    return;
  }

  await msg.react(getCustomEmoji('tildey'));
}
