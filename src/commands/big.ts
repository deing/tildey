/* istanbul ignore file */

import { Attachment, Message } from 'discord.js';
import { getCustomEmoji } from '../utils';

export async function bigCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length === 0) {
    await msg.reply(`You forgot to pass arguments!~ ${getCustomEmoji('pwease')}`);
    return;
  }

  if (args.length > 3) {
    await msg.reply(`I can't send more than three emoji at once!~ ${getCustomEmoji('pwease')}`);
    return;
  }

  const attachments: Attachment[] = [];
  for (const arg of args) {
    if (/<a?:[\w-]+:\d+>/.test(arg)) {
      const extension: string = arg.includes('<a:') ? 'gif' : 'png';
      const emojiID: any = /:(\d+)/.exec(arg)[1];
      attachments.push(new Attachment(`https://cdn.discordapp.com/emojis/${emojiID}.${extension}`));
    }
  }

  if (attachments.length === 0) {
    await msg.reply(`None of these look like custom emoji!~ ${getCustomEmoji('pwease')}`);
  } else {
    await msg.channel.send({ files: attachments });
  }
}
