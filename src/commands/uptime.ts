/* istanbul ignore file */

import { Message } from 'discord.js';
import { getCustomEmoji, pluralize } from '../utils';

export async function uptimeCmd(msg: Message): Promise<void> {
  const uptimes: string[] = [];
  let { uptime } = msg.client;

  // Format the Milliseconds into something more human-readable:
  // 86400000ms = 1d, 3600000ms = 1h, 60000ms = 1m, 1000ms = 1s
  if (uptime >= 86400000) {
    const days = Math.floor(uptime / 86400000);
    uptimes.push(pluralize('day', 's', days));
    uptime %= 86400000;
  }

  if (uptime >= 3600000) {
    const hours = Math.floor(uptime / 3600000);
    uptimes.push(pluralize('hour', 's', hours));
    uptime %= 3600000;
  }

  if (uptime >= 60000) {
    const minutes = Math.floor(uptime / 60000);
    uptimes.push(pluralize('minute', 's', minutes));
    uptime %= 60000;
  }

  if (uptime >= 1000) {
    const seconds = Math.floor(uptime / 1000);
    uptimes.push(pluralize('second', 's', seconds));
  }

  await msg.reply(`I've been online for ${uptimes.join(', ')}!~ ${getCustomEmoji('tildey')}`);
}
