/* istanbul ignore file */

import { resolve } from 'path';
import { Message, DMChannel, TextChannel, User, Guild, RichEmbed } from 'discord.js';
import fecha from 'fecha';
import { ensureDir, readFile } from 'fs-extra';
import { Document, Model, model } from 'mongoose';
import { DefaultLogFields } from 'simple-git/typings/response';
import { quoteSchema } from '../db';
import { RestartMessage } from '../interfaces';
import { getCustomEmoji, getLatestCommit, restart, version, updatePresence } from '../utils';

export async function superCmd(msg: Message, name: string, args: string[]): Promise<void> {
  const supers: string[] = process.env.TLDY_SUPERS.split(',');
  if (!supers.includes(msg.author.id)) {
    await msg.reply('you\'re not authorized to do that!~ :no_entry_sign:');
    return Promise.resolve();
  }

  if (args.length === 0) {
    await msg.reply(`you forgot to include a command!~ ${getCustomEmoji('pwease')}`);
    return Promise.resolve();
  }

  const command = args[0].toLowerCase();
  if (command === 'restart') {
    const newMsg: Message = await msg.reply('restarting… :thinking:') as Message;
    const tempDir = resolve('temp');
    await ensureDir(tempDir);

    const commit: DefaultLogFields = await getLatestCommit();
    const data: RestartMessage = {
      authorID: msg.author.id,
      channelID: newMsg.channel.id,
      messageID: newMsg.id,
      initTime: msg.createdAt,
      version,
      commit: commit.hash.slice(0, 8)
    };
    restart(data);
  } else if (command === 'logs') {
    const channel: DMChannel = await msg.author.createDM();
    let logOutput = '';

    if (args.length >= 2 && args[1].toLowerCase() === 'error') {
      logOutput = await readFile(resolve('logs/error.log'), 'UTF8');
    } else {
      logOutput = await readFile(resolve('logs/combined.log'), 'UTF8');
    }

    // Start at -1 because we're going to remove a line from the message later
    let logLines = -1;
    let logMsg = '';
    for (const logLine of logOutput.split('\n').reverse()) {
      if (logMsg.length + logLine.length < 1950) {
        logMsg += logLine + '\n';
        logLines++;
      } else {
        break;
      }
    }

    // Because we reversed the order to get the latest lines, reverse it again so it's ordered like in the file
    // We're also removing the first line here because Winston leaves an empty newline at the EOF
    logMsg = logMsg.split('\n').splice(1, logMsg.length).reverse().join('\n');

    await channel.send(`Last ${logLines} lines:\n\`\`\`accesslog${logMsg}\`\`\``);
    await msg.react(getCustomEmoji('tildey'));
  } else if (command === 'test') {
    await msg.react(getCustomEmoji('tildey'));
  } else if (command === 'presence') {
    updatePresence(msg.client);
    await msg.react(getCustomEmoji('tildey'));
  } else if (command === 'quote' && args[1].toLowerCase() === 'remove' && args.length === 4 && !isNaN(parseInt(args[3], 10))) {
    const quotes: Model<Document> = model('Quote', quoteSchema);
    const quote: Document = await quotes.findOne({ guild: args[2] });
    if (quote) {
      const quotesArray: any[] = quote.get('quotes');
      if (quotesArray[Number(args[3])]) {
        const { content } = quotesArray[Number(args[3])];
        quotesArray[Number(args[3])].author = '[Redacted]';
        quotesArray[Number(args[3])].content = ` ${content.replace(/\S/g, '█').slice(0, 200)}`;
        await quote.save();
        await msg.react(getCustomEmoji('tildey'));
      } else {
        await msg.reply(`I couldn't find a quote with that ID in the specified server!~ ${getCustomEmoji('pwease')}`);
      }
    } else {
      await msg.reply(`I couldn't find a server with that ID!~ ${getCustomEmoji('pwease')}`);
    }
  } else if (command === 'feedback' && args.length > 2 && args[1].toLowerCase() === 'reply') {
    const feedbackChannel: TextChannel = msg.client.channels.find((val) => val.id === process.env.TLDY_FEEDBACK_CHANNEL) as TextChannel;
    if (feedbackChannel === null) {
      await msg.reply(`I couldn't find the feedback channel!~ ${getCustomEmoji('pwease')}`);
      return;
    }

    let feedbackMessage: Message;
    try {
      feedbackMessage = await feedbackChannel.fetchMessage(args[2]);
    } catch {
      await msg.reply(`I couldn't find that feedback message!~ ${getCustomEmoji('pwease')}`);
      return;
    }

    if (feedbackMessage.content.includes(' in a private message:')) {
      const userTag: string = feedbackMessage.content.slice(0, feedbackMessage.content.indexOf(' in a private message:'));
      const user: User = msg.client.users.find((val) => val.tag === userTag);
      if (user === null) {
        await msg.reply(`I couldn't find a user with the tag \`${userTag}\`!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      // Remove the user and location info from the feedback message so we get the original feedback message
      let originalMessage: string = feedbackMessage.content.slice(feedbackMessage.content.indexOf(' in a private message:') + ' in a private message:'.length);
      // And then add an ellipsis if it's over 100 characters
      originalMessage = originalMessage.length > 100 ?
        originalMessage.slice(0, 100) + '…' :
        originalMessage;
      const embed: RichEmbed = new RichEmbed()
        .setTitle(`${getCustomEmoji('tildey')} Feedback Reply!~`)
        .setColor('f1fa8c')
        .addField(
          'Feedback',
          `On ${fecha.format(new Date(), 'YYYY-MM-DD HH:mm')} you sent this message as feedback:\n` +
          originalMessage
        )
        .addField(
          'Reply',
          `This message was sent by ${msg.author.tag} as response:\n` +
          args.slice(3).join(' ')
        );
      await user.send(`<@${user.id}>, you have mail!~ ${getCustomEmoji('tildey')}`, embed);
      await msg.react(getCustomEmoji('tildey'));
      await feedbackMessage.react('💌');
    } else {
      const userTag: string = feedbackMessage.content.slice(0, feedbackMessage.content.indexOf(' in "'));
      const user: User = msg.client.users.find((val) => val.tag === userTag);
      if (user === null) {
        await msg.reply(`I couldn't find a user with the tag \`${userTag}\`!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      const guildChannelString: string = feedbackMessage.content.slice(
        feedbackMessage.content.indexOf(' in "') + ' in "'.length,
        feedbackMessage.content.indexOf('": ')
      );
      const guildName: string = guildChannelString.slice(0, guildChannelString.lastIndexOf('#'));
      const guild: Guild = msg.client.guilds.find((val) => val.name === guildName);
      if (guild === null) {
        await msg.reply(`I couldn't find the \`${guildName}\` server!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      const channelName: string = guildChannelString.slice(guildChannelString.lastIndexOf('#') + 1);
      const channel: TextChannel = guild.channels.find((val) => val.name === channelName && val.type === 'text') as TextChannel;
      if (channel === null) {
        await msg.reply(`I couldn't find the \`${channelName}\` text channel in the \`${guildName}\` server!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      let originalMessage: string = feedbackMessage.content.slice(feedbackMessage.content.indexOf('": ') + '": '.length);
      originalMessage = originalMessage.length > 100 ?
        originalMessage.slice(0, 100) + '…' :
        originalMessage;
      const embed: RichEmbed = new RichEmbed()
        .setTitle(`${getCustomEmoji('tildey')} Feedback Reply!~`)
        .setColor('f1fa8c')
        .addField(
          'Feedback',
          `On ${fecha.format(feedbackMessage.createdTimestamp, 'YYYY-MM-DD HH:mm')} you sent this message as feedback:\n` +
          originalMessage
        )
        .addField(
          'Reply',
          `This message was sent by ${msg.author.tag} as response:\n` +
          args.slice(3).join(' ')
        );
      // Send the embed to the person that sent feedback
      await channel.send(`<@${user.id}>, you have mail!~ ${getCustomEmoji('tildey')}`, embed);
      // React to the message that invoked the feedback reply command
      await msg.react(getCustomEmoji('tildey'));
      // Add a reaction to the message in the feedback channel so we know it's been replied to
      await feedbackMessage.react('💌');
    }
  } else {
    await msg.reply(`that's not how you use the Super command!~ ${getCustomEmoji('pwease')}`);
  }
}
