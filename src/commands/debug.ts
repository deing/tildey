/* istanbul ignore file */

import { Emoji, GuildMember, Message, RichEmbed, User } from 'discord.js';
import { CustomEmojis, getCustomEmoji, getGuildMember, getLastMessage, getUser, version, log } from '../utils';

export async function debugCmd(msg: Message, name: string, args: string[]): Promise<void> {
  const embed: RichEmbed = new RichEmbed()
    .setColor('#f8f8f2')
    .setTitle('Debug')
    .addField('Command Name', `\`${name}\``)
    .addField('Arguments', `\`${args.join('` - `')}\``);

  if (args.length === 1) {
    if (args[0].toLowerCase() === 'customemojis') {
      embed.addField('CustomEmojis', CustomEmojis.array().join(' '));
    } else if (args[0].toLowerCase() === 'version') {
      embed.addField('Version', version);
    } else if (args[0].toLowerCase() === 'throw') {
      await msg.react(getCustomEmoji('tildey'));
      log.error(`[DEBUG] Debug test error by ${msg.author.tag}`);
      throw new Error(`Debug test error by ${msg.author.tag}`);
    }
  }

  if (args.length >= 2) {
    if (args[0].toLowerCase() === 'getuser') {
      const users: string[] = [];
      for (const arg of args.slice(1, args.length)) {
        const user: User = getUser(msg, arg, true);
        if (user === null) {
          users.push(`\`${arg}\` not a user`);
        } else {
          users.push(`\`${user.id}\` ${user.tag}`);
        }
      }

      embed.addField('getUser', users.join('\n'));
    } else if (args[0].toLowerCase() === 'getguildmember') {
      const users: string[] = [];
      for (const arg of args.slice(1, args.length)) {
        const user: GuildMember = getGuildMember(msg, arg, true);
        if (user === null) {
          users.push(`\`${arg}\` not a user`);
        } else {
          users.push(`\`${user.id}\` ${user.user.tag}`);
        }
      }

      embed.addField('getGuildMember', users.join('\n'));
    } else if (args[0].toLowerCase() === 'getcustomemoji') {
      const emojis: string[] = [];
      for (const arg of args.slice(1, args.length)) {
        const emoji: Emoji|string = getCustomEmoji(arg.toLowerCase());
        if (emoji === ':white_large_square:') {
          emojis.push(`\`${arg}\` ${emoji} (not found)`);
        } else {
          emojis.push(`\`${arg}\` ${emoji}`);
        }
      }

      embed.addField('getCustomEmoji', emojis.join('\n'));
    } else if (args[0].toLowerCase() === 'getlastmessage') {
      const lastMessages: string[] = [];
      for (const arg of args.slice(1, args.length)) {
        const user: User = getUser(msg, arg, true);
        if (user === null) {
          lastMessages.push(`\`${arg}\` user not found`);
        } else {
          const lastMessage: Message = await getLastMessage(msg, user.id);
          if (typeof lastMessage === 'undefined') {
            lastMessages.push(`\`${arg}\` ${user.tag}'s last message not found`);
          } else {
            lastMessages.push(`\`${arg}\` ${user.tag} ${lastMessage.content}`);
          }
        }
      }

      embed.addField('getLastMessage', lastMessages.join('\n'));
    }
  }

  await msg.channel.send(embed);
}
