/* istanbul ignore file */

import { Message, User } from 'discord.js';
import { Document, Model, model } from 'mongoose';
import { getCustomEmoji, getUser, getLastMessage } from '../utils';
import { karmaSchema } from '../db';

export async function exemplaryCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length === 0) {
    await msg.reply(`you forgot to include someone to Exemplary!~ ${getCustomEmoji('pwease')}`);
    return Promise.resolve();
  }

  const noDuplicates: User[] = [];
  for (const arg of args) {
    const user: User = getUser(msg, arg, true);
    if (user !== null && !noDuplicates.includes(user)) {
      noDuplicates.push(user);
    }
  }

  if (noDuplicates.length === 0) {
    await msg.reply('I couldn\'t find anyone by that name in this server!~ :shrug:');
    return Promise.resolve();
  }

  if (noDuplicates.some((user: User) => user.id === msg.author.id)) {
    await msg.reply(`you can't Exemplary yourself!~ ${getCustomEmoji('pwease')}`);
    return Promise.resolve();
  }

  const karmas: Model<Document> = model('Karma', karmaSchema);
  for (const user of noDuplicates) {
    let karma: Document = await karmas.findOne({ guild: msg.guild.id });
    if (!karma) {
      karma = await karmas.create({ guild: msg.guild.id });
    }

    const userKarmas: any[] = karma.get('karmas');
    let userKarma: any = userKarmas.find((val: any) => val.user === user.id);
    if (typeof userKarma === 'undefined') {
      userKarma = {
        user: user.id,
        exemplary: 1,
        malice: 0
      };
      userKarmas.push(userKarma);
    } else {
      userKarma.exemplary++;
    }

    const lastMessage: Message = await getLastMessage(msg, user.id);
    if (lastMessage !== null) {
      await lastMessage.react(getCustomEmoji('exemplary')).catch((error: Error) => {
        if (error.message.toLowerCase().includes('reaction blocked')) {
          msg.react(getCustomEmoji('tildey'));
        }
      });
    }

    await karma.save();
  }

  return Promise.resolve();
}
