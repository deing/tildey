/* istanbul ignore file */

import { Message } from 'discord.js';
import { getCustomEmoji } from '../utils';

// Handling emojis in discord is a nightmare and since we're also using :keycap_ten: we can't use the numberToWords package for this
// so we just have an array of objects with the URL-encoded emojis (for reactions) and the standard ones (for the poll message)
const numbers = [
  { url: '1%E2%83%A3', name: ':one:' },
  { url: '2%E2%83%A3', name: ':two:' },
  { url: '3%E2%83%A3', name: ':three:' },
  { url: '4%E2%83%A3', name: ':four:' },
  { url: '5%E2%83%A3', name: ':five:' },
  { url: '6%E2%83%A3', name: ':six:' },
  { url: '7%E2%83%A3', name: ':seven:' },
  { url: '8%E2%83%A3', name: ':eight:' },
  { url: '9%E2%83%A3', name: ':nine:' },
  { url: '%F0%9F%94%9F', name: ':keycap_ten:' }
];

export async function pollCmd(msg: Message, name: string, args: string[]): Promise<void> {
  // Arguments get split before command execution using spaces so we join them back with spaces and then seperate by comma
  // We also filter out any options that are empty
  const pollOptions: string[] = args.join(' ').replace(/[~*_]/g, ' ').trim().split(',').filter((option) => option.replace(/ /g, '') !== '');

  if (args.length < 3) {
    await msg.reply(`you have to include a poll title and at least 2 options!~ ${getCustomEmoji('tildey')}`);
    return;
  }

  // Take the first argument in pollOptions and capitalize every word's first letter
  const pollTitle: string = pollOptions.shift().toLowerCase().split(' ').map((s) => s.charAt(0).toUpperCase() + s.slice(1)).join(' ');

  let pollMessage = `# Poll: **${pollTitle}**\n`;

  // Maximum length of allowed emojis in the numbers array is allowed, more than that will cause errors
  if (pollOptions.length > numbers.length) {
    await msg.reply(`you included more than ${numbers.length} options in your poll!~ ${getCustomEmoji('tildey')}`);
    return;
  }

  for (const [i, element] of pollOptions.entries()) {
    pollMessage += `${numbers[i].name} - \`Option ${String(i + 1).padStart(2, '0')}\` | **${element}**\n`;
  }

  const newMsg: Message = await msg.channel.send(pollMessage) as Message;
  // Because we want each reaction to be added synchronously, we need to wait for each promise to resolve before starting the next
  for (let i = 0; i < pollOptions.length; i++) {
    await newMsg.react(numbers[i].url);
  }
}
