/* istanbul ignore file */

import { Message } from 'discord.js';
import { getCustomEmoji, log } from '../utils';

const maxLimit = 120;

export async function quickPollCmd(msg: Message, name: string, args: string[]): Promise<void> {
  let timeLimit = 60;

  if (args.length === 0) {
    await msg.reply(`${getCustomEmoji('tildey')} You forgot to add a poll question!~`);
    return;
  }

  try {
    await msg.react(getCustomEmoji('vote_for'));
    await msg.react(getCustomEmoji('vote_against'));
  } catch (error) {
    log.error(`[QUICKPOLL] ${error}`);
  }

  let pollQuestion: string = args.join(' ').trim();

  if (typeof Number(args[0]) === 'number' &&
    Number(args[0]) <= maxLimit &&
    Number(args[0]) > 0) {
    timeLimit = Number(args[0]);
    pollQuestion = args.join(' ').slice(args[0].length, args.join(' ').length).trim();
  }

  setTimeout(() => {
    const votes = {
      for: 0,
      against: 0
    };

    for (const reaction of msg.reactions.array()) {
      // Reduce the reaction.count by 1 because Tildey's initial reactions don't count
      if (reaction.emoji.name === 'vote_for') {
        votes.for = reaction.count - 1;
      } else if (reaction.emoji.name === 'vote_against') {
        votes.against = reaction.count - 1;
      }
    }

    let winningMessage = '';
    winningMessage += `<@${msg.author.id}> asked "${pollQuestion}"`;
    winningMessage += `\n${votes.for} voted in favor… `;
    winningMessage += `${votes.against} voted against…`;

    if (votes.for === votes.against) {
      winningMessage += '\nThe poll is a draw! :scales:';
    } else if (votes.for > votes.against) {
      winningMessage += `\nThe poll says yes! ${getCustomEmoji('vote_for')}`;
    } else if (votes.for < votes.against) {
      winningMessage += `\nThe poll says no! ${getCustomEmoji('vote_against')}`;
    }

    msg.channel.send(winningMessage);
  }, 1000 * timeLimit);
}
