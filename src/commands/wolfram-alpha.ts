/* istanbul ignore file */

import { Message, RichEmbed } from 'discord.js';
import fecha from 'fecha';
import { WolframClient } from 'node-wolfram-alpha';
import { getCustomEmoji } from '../utils';

export async function wolframAlphaCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (process.env.TLDY_WA_TOKEN === '') {
    await msg.reply('that command is disabled!~ :no_entry_sign:');
    return Promise.resolve();
  }

  const wolfram: WolframClient = new WolframClient(process.env.TLDY_WA_TOKEN);
  msg.channel.startTyping();
  const query: string = args.join(' ').trim();
  const result = await wolfram.query(query, { format: 'plaintext', podindex: '1,2' });
  msg.channel.stopTyping();

  if (!result.data.queryresult.success) {
    await msg.reply(`${getCustomEmoji('pwease')} That's not a valid Wolfram|Alpha query!~`);
    return;
  }

  if (result.data.queryresult.error) {
    await msg.reply(`${getCustomEmoji('pwease')} Something went wrong with querying Wolfram|Alpha!~`);
    return;
  }

  const inputData: string = result.data.queryresult.pods[0].subpods[0].plaintext;
  const outputData: string = result.data.queryresult.pods[1].subpods[0].plaintext;
  if (inputData.length === 0 || outputData.length === 0) {
    await msg.reply(`${getCustomEmoji('pwease')} I got an empty result from Wolfram|Alpha that can't be displayed!~`);
    return;
  }

  const embed: RichEmbed = new RichEmbed()
    .setTitle(`${getCustomEmoji('wolfram')} Wolfram|Alpha`)
    .setColor('#dd1100')
    .addField('Input', inputData)
    .addField('Result', outputData)
    .setFooter(`Query — ${fecha.format(new Date(), 'YYYY-MM-DD HH:mm:ss')}`);

  await msg.channel.send(embed);
  return Promise.resolve();
}
