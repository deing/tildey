/* istanbul ignore file */

import { Message, Role, DMChannel } from 'discord.js';
import { toWords } from 'number-to-words';
// @ts-ignore Ignore a "no declarations" error
import twemoji from 'twemoji';
import { getCustomEmoji, getRole, getGuildMember } from '../utils';

// All the characters that we know have a corresponding emoji (like 🇪)
const approvedCharacters: {letters: string; numbers: string; special: Array<{char: string; replace: string}>} = {
  letters: 'abcdefghijklmnopqrstuvwxyz',
  numbers: '0123456789',
  special: [
    {
      char: '#',
      replace: 'hash'
    },
    {
      char: '*',
      replace: 'asterisk'
    }
  ]
};

export async function spellCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length === 0) {
    await msg.reply(`you forgot to include something to spell!~ ${getCustomEmoji('pwease')}`);
    return;
  }

  for (let i = 0; i < args.length; i++) {
    if (msg.guild) {
      if (/<#.*>/.exec(args[i])) {
        args[i] = `#${msg.guild.channels.get(args[i].replace(/[<#>]/g, '')).name}`;
      } else if (/<@&.*>/.exec(args[i])) {
        const role: Role = getRole(msg, args[i]);
        args[i] = role === null ? '' : role.name;
      } else if (/<@!?.*>/.exec(args[i])) {
        args[i] = getGuildMember(msg, args[i]).displayName;
      }
    } else if (msg.channel instanceof DMChannel) {
      args[i] = msg.client.user.tag;
    }
  }

  // Remove all special discord formatting not converted before
  args = args.filter((val: string) => !val.match(/<[:#@!&]?.*>/g));
  let spellText = '';
  for (const char of args.join(' ')) {
    if (char.toLowerCase() === 'b') {
      spellText += ':b:';
    } else if (approvedCharacters.letters.includes(char.toLowerCase())) {
      spellText += `:regional_indicator_${char.toLowerCase()}:\u200C`;
    } else if (approvedCharacters.numbers.includes(char)) {
      spellText += `:${toWords(Number(char))}:`;
    } else if (char === ' ') {
      // If the character is a space, we want to replace it with an "em space" to give it some extra width
      spellText += '\u2003';
    } else if (char === '\n') {
      // If the character is a newline, we also want to add that
      spellText += '\n';
    }

    // Iterate over the special character list and add them to the spell text when appropriate
    for (const special of approvedCharacters.special) {
      if (special.char === char) {
        spellText += `:${special.replace}:`;
        break;
      }
    }

    // If Twemoji (Discord uses Twemoji) can parse it, it will give us the unicode for the emoji
    twemoji.parse(char, (icon: string) => {
      try {
        // Then we need to convert it back to an emoji which we can do by parsing int and creating a string from that codepoint
        spellText += String.fromCodePoint(parseInt(icon, 16));
      } catch {
        // With some emojis that specify sex/skin tone fromCodePoint will error out so we wanna catch that
      }
    });
  }

  // Trim unnecessary em spaces in the resulting message
  spellText = spellText.replace(/\u2003+/g, '\u2003');

  if (spellText.length === 0) {
    await msg.reply(`I found no characters to convert!~ ${getCustomEmoji('pwease')}`);
    return;
  }

  await msg.channel.send(spellText);
}
