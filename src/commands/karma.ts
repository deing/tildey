/* istanbul ignore file */

import { Message, User, RichEmbed } from 'discord.js';
import { Document, Model, model } from 'mongoose';
import { karmaSchema } from '../db';
import { getUser, getCustomEmoji } from '../utils';

const karmaMessages: string[][] = [
  ['neutral', 'accepted', 'liked', 'idolized'],
  ['shunned', 'mixed', 'a smiling troublemaker', 'a good-natured rascal'],
  ['hated', 'a sneering punk', 'unpredictable', 'a dark hero'],
  ['vilified', 'a merciful thug', 'a soft-hearted devil', 'a wild child']
];

export async function karmaCmd(msg: Message, name: string, args: string[]): Promise<void> {
  let targetUser: User = msg.author;
  if (args[0]) {
    targetUser = getUser(msg, args[0]);
    if (targetUser === null) {
      return Promise.resolve();
    }
  }

  const karmas: Model<Document> = model('Karma', karmaSchema);
  const karma: Document = await karmas.findOne({ guild: msg.guild.id });
  let exemplary = 0;
  let malice = 0;
  if (karma) {
    const userKarma: any = karma.get('karmas').find((val: any) => val.user === targetUser.id);
    if (typeof userKarma !== 'undefined') {
      exemplary = userKarma.exemplary;
      malice = userKarma.malice;
    }
  }

  const exemplaryScore: number = karmaScoring(exemplary);
  const maliceScore: number = karmaScoring(malice);
  let karmaText = '';
  if (exemplaryScore >= 4 || maliceScore >= 4) {
    karmaText += '**a power user™**';
  } else {
    karmaText += karmaMessages[maliceScore][exemplaryScore];
  }

  const netKarma: number = exemplary - malice;
  const karmaEmbed: RichEmbed = new RichEmbed()
    .setThumbnail(targetUser.displayAvatarURL)
    .setTitle(`${targetUser.username}'s karma on ${msg.guild.name}`)
    .addField(getCustomEmoji('exemplary'), exemplary, true)
    .addField(getCustomEmoji('malice'), malice, true)
    .addField(`Total Karma: ${netKarma}`, `${targetUser.username} is seen as ${karmaText}.`, false)
    .setColor((netKarma < 0 ? '#dc322f' : '#278cd3'));

  await msg.channel.send(karmaEmbed);
  return Promise.resolve();
}

function karmaScoring(raw: number): number {
  if (raw <= 5) {
    return 0;
  }

  if (raw <= 20) {
    return 1;
  }

  if (raw <= 50) {
    return 2;
  }

  if (raw <= 199) {
    return 3;
  }

  return 4;
}
