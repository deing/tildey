/* istanbul ignore file */

import { Guild, Client } from 'discord.js';
import { log, updatePresence } from '../utils';

export function clientGuildDelete(client: Client, guild: Guild): void {
  updatePresence(client);
  log.info(`[LEFTGUILD] Left guild "${guild.name}"`);
}
