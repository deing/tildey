/* istanbul ignore file */

import { log } from '../utils';

export function clientError(error: Error): void {
  log.error(`[DISCORD] ${error.message}:\n${error.stack}`);
}
