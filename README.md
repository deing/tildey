<img src="images/tildey.png" height="128" align="right">

# Tildey

> A Discord bot, because why not.

---

[![Discord](https://img.shields.io/discord/511723413602304021.svg?color=%237289DA&label=Tildey%27s%20Sandbox&style=flat-square)](https://discord.gg/yn8E3m4) [![Ko-Fi](https://flat.badgen.net/badge/Support%20Me%20On/Ko-Fi/FF5E5B)](https://ko-fi.com/tildey) [![Language](https://flat.badgen.net/badge/Language/TypeScript/007acc)](https://www.typescriptlang.org/) [![Version](https://flat.badgen.net/badge/Version/1.5.0/green)](package.json#L3) [![Pipeline](https://gitlab.com/tildey/tildey/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/tildey/tildey/pipelines) [![Coverage](https://img.shields.io/codecov/c/gitlab/tildey/tildey/master.svg?style=flat-square)](https://codecov.io/gl/tildey/tildey)

## Discord Server

Tildey has her own server, if you have any questions or feedback feel free to join. [discord.gg/yn8E3m4](https://discord.gg/yn8E3m4)

## Contributing

Want to contribute to Tildey? Check out the [Contributing](CONTRIBUTING.md) document.

## Tildes

Tildey was initially created for an unofficial [Tildes](https://tildes.net) Discord server, but quickly turned into a more general purpose bot.

## License

Licensed under [AGPL-3.0-or-later](LICENSE).
