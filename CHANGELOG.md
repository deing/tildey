# [1.5.0](https://gitlab.com/tildey/tildey/compare/v1.4.0...v1.5.0) (2019-09-10)


### Bug Fixes

* 'successful' typo ([56ec2f8](https://gitlab.com/tildey/tildey/commit/56ec2f8))
* add webhook testing (fixes [#121](https://gitlab.com/tildey/tildey/issues/121)) ([24a6f4c](https://gitlab.com/tildey/tildey/commit/24a6f4c))
* allow pipeline jobs to be skipped ([0e636e1](https://gitlab.com/tildey/tildey/commit/0e636e1))
* always run tests in UTC timezone ([c497f06](https://gitlab.com/tildey/tildey/commit/c497f06))
* only run tildey-specific pipelines in the tildey project and allow tests to run anywhere ([fc20a5d](https://gitlab.com/tildey/tildey/commit/fc20a5d))
* update gitlab shared utils tests ([909826c](https://gitlab.com/tildey/tildey/commit/909826c))


### Features

* add a restart webhook (fixes [#120](https://gitlab.com/tildey/tildey/issues/120)) ([5939d54](https://gitlab.com/tildey/tildey/commit/5939d54))
* add manual deploy job ([28aecad](https://gitlab.com/tildey/tildey/commit/28aecad))



# [1.4.0](https://gitlab.com/tildey/tildey/compare/v1.3.0...v1.4.0) (2019-09-07)


### Bug Fixes

* add label to all log messages (fixes [#117](https://gitlab.com/tildey/tildey/issues/117)) ([1d227f1](https://gitlab.com/tildey/tildey/commit/1d227f1))
* force recent and loved tracks to only display 5 maximum ([797228c](https://gitlab.com/tildey/tildey/commit/797228c))
* make sure only success or failed pipelines pass through ([2101aaa](https://gitlab.com/tildey/tildey/commit/2101aaa))
* remove stray debug log ([6036194](https://gitlab.com/tildey/tildey/commit/6036194))
* truncate log executed command to 25 characters max and replace newlines with linefeed character (fixes [#111](https://gitlab.com/tildey/tildey/issues/111)) ([3be6f08](https://gitlab.com/tildey/tildey/commit/3be6f08))


### Features

* add enabling/disabling commands again (fixes [#59](https://gitlab.com/tildey/tildey/issues/59)) ([c7a29c0](https://gitlab.com/tildey/tildey/commit/c7a29c0))
* add new command ~lastfm (fixes [#41](https://gitlab.com/tildey/tildey/issues/41)) ([5bd8cd9](https://gitlab.com/tildey/tildey/commit/5bd8cd9))
* add role cleanup command (fixes [#83](https://gitlab.com/tildey/tildey/issues/83)) ([489a8b9](https://gitlab.com/tildey/tildey/commit/489a8b9))



# [1.3.0](https://gitlab.com/tildey/tildey/compare/v1.2.0...v1.3.0) (2019-09-04)


### Bug Fixes

* add log rotation back (fixes [#101](https://gitlab.com/tildey/tildey/issues/101)) ([83bcd5d](https://gitlab.com/tildey/tildey/commit/83bcd5d))
* fix scales emoji being output as text ([791ab3c](https://gitlab.com/tildey/tildey/commit/791ab3c))
* make sure rich embed fields aren't over 1024 characters (fixes [#112](https://gitlab.com/tildey/tildey/issues/112)) ([f2681aa](https://gitlab.com/tildey/tildey/commit/f2681aa))
* make webhooks use a single express app with a port specified in .env (fixes [#109](https://gitlab.com/tildey/tildey/issues/109)) ([b045d91](https://gitlab.com/tildey/tildey/commit/b045d91))


### Features

* add gitlab merge request note rate limiting (fixes [#70](https://gitlab.com/tildey/tildey/issues/70)) ([5bc3f47](https://gitlab.com/tildey/tildey/commit/5bc3f47))
* add support for gitlab job and pipeline webhook events (fixes [#98](https://gitlab.com/tildey/tildey/issues/98)) ([d739c50](https://gitlab.com/tildey/tildey/commit/d739c50))



# [1.2.0](https://gitlab.com/tildey/tildey/compare/v1.1.0...v1.2.0) (2019-07-24)


### Bug Fixes

* make development a little easier (merged !47) ([466f96e](https://gitlab.com/tildey/tildey/commit/466f96e))
* ping when pong and pong when ping ([34cf5d3](https://gitlab.com/tildey/tildey/commit/34cf5d3))
* update node-ddg, ddg now properly returns the full links again ([b4e935f](https://gitlab.com/tildey/tildey/commit/b4e935f))


### Features

* **commands:** add sponge and big commands, fixes [#99](https://gitlab.com/tildey/tildey/issues/99) and [#100](https://gitlab.com/tildey/tildey/issues/100) ([e2c7323](https://gitlab.com/tildey/tildey/commit/e2c7323))
* **reminder:** add objective date support ([48d3e77](https://gitlab.com/tildey/tildey/commit/48d3e77))



# [1.1.0](https://gitlab.com/tildey/tildey/compare/v1.0.0...v1.1.0) (2019-05-28)


### Bug Fixes

* add 'v' to version tags link ([dab185d](https://gitlab.com/tildey/tildey/commit/dab185d))
* add yarn dependency for super restart ([035a8ba](https://gitlab.com/tildey/tildey/commit/035a8ba))
* ddg returning an extra https in the link ([ce6615d](https://gitlab.com/tildey/tildey/commit/ce6615d))
* don't let empty arguments pass ([fb02361](https://gitlab.com/tildey/tildey/commit/fb02361))
* find the optout after a user is found ([1d09a66](https://gitlab.com/tildey/tildey/commit/1d09a66))
* fix users not being able to be found across guilds ([1f2511c](https://gitlab.com/tildey/tildey/commit/1f2511c))
* make color output color or colour and use pluralize ([bb7873e](https://gitlab.com/tildey/tildey/commit/bb7873e))
* sanitize mentions out of the feedback message ([88008e5](https://gitlab.com/tildey/tildey/commit/88008e5))
* use npx so yarn doesn't have to be installed directly ([6f77bf0](https://gitlab.com/tildey/tildey/commit/6f77bf0))


### Features

* **new command:** codepoints, list the code points of each character in a message (fixes [#80](https://gitlab.com/tildey/tildey/issues/80)) ([215ac1a](https://gitlab.com/tildey/tildey/commit/215ac1a))
* **new command:** fancify, convert your message to random fancy letters (fixes [#66](https://gitlab.com/tildey/tildey/issues/66)) ([f43ce26](https://gitlab.com/tildey/tildey/commit/f43ce26))
* **new command:** whatis, figure out what a Discord ID is (fixes [#96](https://gitlab.com/tildey/tildey/issues/96)) ([e251206](https://gitlab.com/tildey/tildey/commit/e251206))
* add support for multiple webhooks (fixes [#97](https://gitlab.com/tildey/tildey/issues/97)) ([17fb536](https://gitlab.com/tildey/tildey/commit/17fb536))
* make reminders easier to create ([#85](https://gitlab.com/tildey/tildey/issues/85)) and allow people to list and delete reminders ([#87](https://gitlab.com/tildey/tildey/issues/87)) ([02b11b0](https://gitlab.com/tildey/tildey/commit/02b11b0))



# [1.0.0](https://gitlab.com/tildey/tildey/compare/v0.5.0...v1.0.0) (2019-05-23)


### Bug Fixes

* reimplemented color now using `node-canvas` instead of `svg-to-img`. (fixes #56)
* separated malice and exemplary (fixes #68)
* commands starting with a number (like `~1` `~1a`) will be ignored. (fixes #67)
* when using exemplary/malice, if target user is blocked Tildey will react to the command message (fixes #77)
* empty poll options will now be ignored (fixes #78)
* the hex color for any given quote will now be in the body too (fixes #79)
* karma is now saved in the database similar to quotes (fixes #88)
* added unit testing for a whole bunch of things
* changed to a new command structure


### Features

* added aliases `upvote` for `exemplary` and `downvote` for `malice`. (fixes #64)
* added `super quote remove <guild> <quote>` to redact quotes. (fixes #65)
* love can now spread the love to multiple users at a time (fixes #76)
* the restart message now includes any changes to commit and/or version (fixes #84)
* added `super presence` to rerun the presence function (fixes #91)
* you can now opt out of (and back in) using `whois optout` and `whois optin` (fixes #95)
* added links to the website when using `help`



# [0.5.0](https://gitlab.com/Tildey/tildey/compare/v0.4.0...v0.5.0) (2019-02-19)


### Bug Fixes

* **love:** allow love to handle tags too, [#75](https://gitlab.com/Tildey/tildey/issues/75) ([7a130c8](https://gitlab.com/Tildey/tildey/commit/7a130c8))
* **remind:** prevent integer overflow from happening with very long timeouts ([9bb6594](https://gitlab.com/Tildey/tildey/commit/9bb6594))
* **role:** allow role color to accept hex without # ([635d33f](https://gitlab.com/Tildey/tildey/commit/635d33f))
* fixes an issue where you couldn't use tags anymore with getUser ([ffbfd3c](https://gitlab.com/Tildey/tildey/commit/ffbfd3c))
* fixes an issue where you couldn't use tags anymore with getUser Part 2: Oopsie Woopsie ([558af0c](https://gitlab.com/Tildey/tildey/commit/558af0c))
* fixes getUser to work with raw IDs ([9167b34](https://gitlab.com/Tildey/tildey/commit/9167b34))


### Features

* **quote:** add "quote list @User", [#73](https://gitlab.com/Tildey/tildey/issues/73) ([ec42425](https://gitlab.com/Tildey/tildey/commit/ec42425))



# [0.4.0](https://gitlab.com/Tildey/tildey/compare/v0.3.0...v0.4.0) (2019-02-01)


### Bug Fixes

* prevent infinite message loop from happening when asking for prefix ([5c405a2](https://gitlab.com/Tildey/tildey/commit/5c405a2))
* **karma:** allow multiple exemplaries/malices in one message ([1703d73](https://gitlab.com/Tildey/tildey/commit/1703d73))


### Features

* **new command:** remind me command ([2882139](https://gitlab.com/Tildey/tildey/commit/2882139))



# [0.3.0](https://gitlab.com/Tildey/tildey/compare/v0.2.6...v0.3.0) (2019-01-13)


### Bug Fixes

* Changes personal details to new contact@tildey.xyz details, fixes [#60](https://gitlab.com/Tildey/tildey/issues/60) ([897fbde](https://gitlab.com/Tildey/tildey/commit/897fbde))
* Fix empty merge request descriptions not working, fixes [#54](https://gitlab.com/Tildey/tildey/issues/54) ([ce6cc40](https://gitlab.com/Tildey/tildey/commit/ce6cc40))
* Make ~love unable to react duplicate emojis ([62f6e4b](https://gitlab.com/Tildey/tildey/commit/62f6e4b))
* Temporarily disabled command enabling/disabling ([6614de2](https://gitlab.com/Tildey/tildey/commit/6614de2))


### Features

* Add conventional-changelog generation ([93c57e6](https://gitlab.com/Tildey/tildey/commit/93c57e6))


