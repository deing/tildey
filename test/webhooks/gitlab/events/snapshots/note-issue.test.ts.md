# Snapshot report for `test/webhooks/gitlab/events/note-issue.test.ts`

The actual snapshot is saved in `note-issue.test.ts.snap`.

Generated by [AVA](https://ava.li).

## format note issue returns with a description

> Snapshot 1

    RichEmbed {
      author: undefined,
      color: 15858316,
      description: `**[Bauke](https://gitlab.com/Bauke) commented on issue [#35](https://gitlab.com/tildey/tildey/issues/35):**␊
      Test␊
      [See More…](https://gitlab.com/tildey/tildey/issues/35#note_214140290)`,
      fields: [],
      file: undefined,
      files: [],
      footer: {
        icon_url: undefined,
        text: 'Comment on Issue',
      },
      image: undefined,
      thumbnail: {
        url: undefined,
      },
      timestamp: undefined,
      title: ':white_large_square: GitLab',
      url: undefined,
    }

## format note issue returns with a description over 80 characters

> Snapshot 1

    RichEmbed {
      author: undefined,
      color: 15858316,
      description: `**[Bauke](https://gitlab.com/Bauke) commented on issue [#35](https://gitlab.com/tildey/tildey/issues/35):**␊
      Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test …␊
      [See More…](https://gitlab.com/tildey/tildey/issues/35#note_214140290)`,
      fields: [],
      file: undefined,
      files: [],
      footer: {
        icon_url: undefined,
        text: 'Comment on Issue',
      },
      image: undefined,
      thumbnail: {
        url: undefined,
      },
      timestamp: undefined,
      title: ':white_large_square: GitLab',
      url: undefined,
    }
