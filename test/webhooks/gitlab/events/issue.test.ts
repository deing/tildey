import { join } from 'path';
import test, { ExecutionContext, Macro } from 'ava';
import { RichEmbed } from 'discord.js';
import { readJSON } from 'fs-extra';
import { formatIssue } from '../../../../src/webhooks/gitlab/events/issue';

const macro: Macro<[string]> = async (t: ExecutionContext, dataPath: string): Promise<void> => {
  const embed: RichEmbed = formatIssue(await readJSON(join(__dirname, dataPath)));
  // Remove the current date from the footer otherwise the snapshot will never pass
  embed.setFooter(embed.footer.text.slice(0, embed.footer.text.indexOf(' — ')));
  t.snapshot(embed);
};

test(
  'format issue returns opened with a description',
  macro,
  'data/issue-opened-description.json'
);

test(
  'format issue returns closed with a description',
  macro,
  'data/issue-closed-description.json'
);

test(
  'format issue returns updated with a description',
  macro,
  'data/issue-updated-description.json'
);

test(
  'format issue returns without a description',
  macro,
  'data/issue-no-description.json'
);

test(
  'format issue returns with a description over 1000 characters',
  macro,
  'data/issue-1000-characters.json'
);

test('format issue returns null with unknown action', async (t: ExecutionContext): Promise<void> => {
  const embed: RichEmbed = formatIssue(await readJSON(join(__dirname, 'data/issue-unknown-action.json')));
  t.is(embed, null);
});
