import { join } from 'path';
import test, { ExecutionContext, Macro } from 'ava';
import { RichEmbed } from 'discord.js';
import { readJSON } from 'fs-extra';
import { formatJob } from '../../../../src/webhooks/gitlab/events/job';

const macro: Macro<[string]> = async (t: ExecutionContext, dataPath: string): Promise<void> => {
  const embed: RichEmbed = formatJob(await readJSON(join(__dirname, dataPath)));
  // Remove the current date from the footer otherwise the snapshot won't pass
  embed.setFooter(embed.footer.text.slice(0, embed.footer.text.indexOf(' — ')));
  t.snapshot(embed);
};

test(
  'format job returns with success',
  macro,
  'data/job-success.json'
);

test(
  'format job returns with failed and not allowed to fail',
  macro,
  'data/job-failed.json'
);

test(
  'format job returns with failed and allowed to fail',
  macro,
  'data/job-allowed-failure.json'
);

test('format job returns null with unknown status', async (t: ExecutionContext): Promise<void> => {
  const embed: RichEmbed = formatJob(await readJSON(join(__dirname, 'data/job-unknown-status.json')));
  t.is(embed, null);
});
