import { join } from 'path';
import test, { ExecutionContext, Macro } from 'ava';
import { RichEmbed } from 'discord.js';
import { readJSON } from 'fs-extra';
import { formatPipeline } from '../../../../src/webhooks/gitlab/events';

const macro: Macro<[string]> = async (t: ExecutionContext, dataPath: string): Promise<void> => {
  const embed: RichEmbed = formatPipeline(await readJSON(join(__dirname, dataPath)));
  // Remove the current date from the footer otherwise the snapshot will never pass
  embed.setFooter(embed.footer.text.slice(0, embed.footer.text.indexOf(' — ')));
  t.snapshot(embed);
};

test(
  'format pipeline returns with success',
  macro,
  'data/pipeline-success.json'
);

test(
  'format pipeline returns with failed',
  macro,
  'data/pipeline-failed.json'
);

test('format pipeline returns null with unknown status', async (t: ExecutionContext): Promise<void> => {
  const embed: RichEmbed = formatPipeline(await readJSON(join(__dirname, 'data/pipeline-unknown-status.json')));
  t.is(embed, null);
});

test('format pipeline returns null with unknown build status', async (t: ExecutionContext): Promise<void> => {
  const embed: RichEmbed = formatPipeline(await readJSON(join(__dirname, 'data/pipeline-build-unknown-status.json')));
  t.is(embed, null);
});
