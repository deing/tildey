import { join } from 'path';
import test, { ExecutionContext, Macro } from 'ava';
import { RichEmbed } from 'discord.js';
import { readJSON } from 'fs-extra';
import { formatNoteIssue } from '../../../../src/webhooks/gitlab/events';

const macro: Macro<[string]> = async (t: ExecutionContext, dataPath: string): Promise<void> => {
  const embed: RichEmbed = formatNoteIssue(await readJSON(join(__dirname, dataPath)));
  // Remove the current date from the footer otherwise the snapshot will never pass
  embed.setFooter(embed.footer.text.slice(0, embed.footer.text.indexOf(' — ')));
  t.snapshot(embed);
};

test(
  'format note issue returns with a description',
  macro,
  'data/note-issue.json'
);

test(
  'format note issue returns with a description over 80 characters',
  macro,
  'data/note-issue-long.json'
);
