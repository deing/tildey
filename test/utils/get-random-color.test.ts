import test from 'ava';
import { getRandomColor } from '../../src/utils';
import { isEqual } from '../index.test';

const hexRegex = /^#(?:[0-9a-fA-F]{6})$/;

test(
  'getRandomColor returns valid 6-character hex color', isEqual,
  () => hexRegex.exec(getRandomColor()).length, 1
);
