import test from 'ava';
import { latestReminder } from '../../src/utils';
import { isEqual } from '../index.test';

test(
  'latestReminder is exported properly', isEqual,
  latestReminder.handle, 0
);
