import test from 'ava';
import { Collection, Message, Role, Snowflake } from 'discord.js';
import { getRole } from '../../src/utils';
import { isEqual } from '../index.test';

const roles: Collection<Snowflake, Role> = new Collection();

// "as unknown as ..." allows us to create these objects
// without having to mock all the data that's actually required for them normally
// we only define the properties that getRole will use to find a role
const message: Message = {
  guild: {
    roles
  }
} as unknown as Message;

test.before(() => {
  roles.set('01', {} as unknown as Role);
});

const roleTests: Array<{ title: string; input: () => any; expected: () => any }> = [
  {
    title: 'getRole returns valid role if found',
    input: () => getRole(message, '01', true),
    expected: () => message.guild.roles.get('01')
  },
  {
    title: 'getRole returns null if none found',
    input: () => getRole(message, '00', true),
    expected: null
  }
];

roleTests.map(({ title, input, expected }) => test(title, isEqual, input, expected));
