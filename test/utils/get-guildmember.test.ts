import test from 'ava';
import { Collection, GuildMember, Message, Snowflake } from 'discord.js';
import { getGuildMember } from '../../src/utils';
import { isEqual } from '../index.test';

const members: Collection<Snowflake, GuildMember> = new Collection();

// "as unknown as ..." allows us to create these objects
// without having to mock all the data that's actually required for them normally
// we only define the properties that getGuildMember will use to find a member
const message: Message = {
  guild: {
    members
  }
} as unknown as Message;

test.before(() => {
  members.set('01', {
    user: {
      tag: 'test#0123'
    }
  } as unknown as GuildMember);
});

const guildMemberTests: Array<{ title: string; input: () => any; expected: () => any}> = [
  {
    title: 'getGuildMember returns GuildMember if found with ID',
    input: () => getGuildMember(message, '01', true),
    expected: () => message.guild.members.get('01')
  },
  {
    title: 'getGuildMember returns GuildMember if found with tag',
    input: () => getGuildMember(message, 'test#0123', true),
    expected: () => message.guild.members.get('01')
  },
  {
    title: 'getGuildMember returns null if none found',
    input: () => getGuildMember(message, '00', true),
    expected: null
  }
];

guildMemberTests.map(({ title, input, expected }) => test(title, isEqual, input, expected));
