import test from 'ava';
import { Permissions } from 'discord.js';
import { checkPermissions } from '../../src/utils';
import { isEqual } from '../index.test';

// Define the tests to use with the isEqual Macro
const permissionTests: Array<{ title: string; input: boolean; expected: boolean }> = [
  {
    title: 'checkPermissions returns true if input has admin',
    input: checkPermissions(
      new Permissions(['SEND_MESSAGES', 'ADMINISTRATOR']),
      new Permissions(['SEND_MESSAGES', 'BAN_MEMBERS'])
    ),
    expected: true
  },
  {
    title: 'checkPermissions returns true for good permissions',
    input: checkPermissions(
      new Permissions(['SEND_MESSAGES']),
      new Permissions(['SEND_MESSAGES'])
    ),
    expected: true
  },
  {
    title: 'checkPermissions returns false for bad permissions',
    input: checkPermissions(
      new Permissions(['SEND_MESSAGES']),
      new Permissions(['SEND_MESSAGES', 'BAN_MEMBERS'])
    ),
    expected: false
  }
];

permissionTests.map(({ title, input, expected }) => test(title, isEqual, input, expected));
