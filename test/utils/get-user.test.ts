import test from 'ava';
import { Collection, DMChannel, GuildMember, Message, Snowflake, User } from 'discord.js';
import { getUser } from '../../src/utils';
import { isEqual } from '../index.test';

const members: Collection<Snowflake, GuildMember> = new Collection();

// "as unknown as ..." allows us to create these objects
// without having to mock all the data that's actually required for them normally
// we only define the properties that getUser will use to find a user
const messageGuild: Message = {
  channel: {
    type: 'text'
  },
  client: {
    users: new Collection<string, User>()
  },
  guild: {
    members
  }
} as unknown as Message;

const messageDM: Message = {
  channel: {
    type: 'dm',
    recipient: {
      id: '02'
    }
  }
} as unknown as Message;

const messageOther: Message = {
  channel: {
    type: 'other'
  }
} as unknown as Message;

test.before(() => {
  members.set('01', {
    user: {
      id: '01',
      tag: 'test#0123'
    } as unknown as User
  } as unknown as GuildMember);
  messageGuild.client.users.set('02', {
    id: '02',
    tag: 'test#4567'
  } as unknown as User);
});

const userTests: Array<{ title: string; input: () => any; expected: () => any }> = [
  {
    title: 'getUser returns User if found with ID in Guild',
    input: () => getUser(messageGuild, '01', true),
    expected: () => messageGuild.guild.members.get('01').user
  },
  {
    title: 'getUser returns User if found in client\'s users Collection',
    input: () => getUser(messageGuild, '02', true),
    expected: () => messageGuild.client.users.get('02')
  },
  {
    title: 'getUser returns User if found with Tag in Guild',
    input: () => getUser(messageGuild, 'test#0123', true),
    expected: () => messageGuild.guild.members.get('01').user
  },
  {
    title: 'getUser returns null if none found in Guild',
    input: () => getUser(messageGuild, '00', true),
    expected: null
  },
  {
    title: 'getUser returns recipient User if in DM',
    input: () => getUser(messageDM, '00', true),
    expected: () => (messageDM.channel as DMChannel).recipient
  },
  {
    title: 'getUser returns null for other channel types',
    input: () => getUser(messageOther, '00', true),
    expected: null
  }
];

userTests.map(({ title, input, expected }) => test(title, isEqual, input, expected));
