import test from 'ava';
import { version as versionInput } from '../../src/utils';
import { version as versionExpected } from '../../package.json';
import { isEqual } from '../index.test';

test('version is loaded and exported properly', isEqual, versionInput, versionExpected);
