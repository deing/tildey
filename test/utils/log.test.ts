import test from 'ava';
import { initLog, log } from '../../src/utils';
import { isEqual } from '../index.test';

test.before(() => {
  initLog();
});

const logTests: Array<{ title: string; input: () => any; expected: any }> = [
  {
    title: 'log is set to info in production',
    input: () => log.level,
    expected: 'info'
  },
  {
    title: 'log is set to silly in development',
    input: () => {
      process.env.TLDY_ENV = 'development';
      initLog();
      return log.level;
    },
    expected: 'silly'
  }
];

// Make these tests serial because they will change the logger in the process
logTests.map(({ title, input, expected }) => test.serial(title, isEqual, input, expected));
