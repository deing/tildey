import { resolve } from 'path';
import test, { ExecutionContext } from 'ava';
import { unlinkSync, writeFileSync } from 'fs-extra';
import { initEnv } from '../../src/utils/env';
import { envThrows } from '../index.test';

// Sample variables to not have to type the same stuff over and over
const sEnv = 'TLDY_ENV=\'development\'\n';
const sEmojiGuild = 'TLDY_EMOJIS_GUILD=\'123\'\n';
const sFeedbackChannel = 'TLDY_FEEDBACK_CHANNEL=\'xyz\'\n';
const sPrefix = 'TLDY_PREFIX=\'~\'\n';
const sToken = 'TLDY_TOKEN=\'abc\'\n';
const sSupers = 'TLDY_SUPERS=\'789,123\'\n';

// Define the tests to use with the envThrows Macro
// the other tests are run manually below
const envTests: Array<{ title: string; env: string; error: string }> = [
  {
    title: 'throws on TLDY_PREFIX=\'prefix\'',
    env: 'TLDY_PREFIX=\'prefix\'\n' + sToken + sEnv + sEmojiGuild + sFeedbackChannel + sSupers,
    error: 'TLDY_PREFIX should probably not be "prefix"'
  },
  {
    title: 'throws on TLDY_TOKEN=\'token\'',
    env: sPrefix + 'TLDY_TOKEN=\'token\'\n' + sEnv + sEmojiGuild + sFeedbackChannel + sSupers,
    error: 'TLDY_TOKEN has to be a Discord bot token https://discordapp.com/developers/docs/intro#bots-and-apps'
  }
];

// Test a non-existent random filename that shouldn't exist
// The only env files that should ever exist are `.env` and `.env.example`
test('throws on non-existent .env', (t: ExecutionContext) => {
  const randomEnv = `.env.${Math.floor(Math.random() * 1000)}`;
  t.throws(
    () => initEnv(randomEnv),
    `\`${randomEnv}\` does not exist, please create one using \`.env.example\``
  );
});

// Iterate through predefined tests and run them
envTests.map(({ title, env, error }) => test(title, envThrows, env, error));

test('joi throws on invalid .env', (t: ExecutionContext) => {
  const env = sPrefix;
  writeFileSync(resolve('.env.test'), env);
  t.throws(() => initEnv('.env.test'));
});

// Test a valid .env manually making sure it doesn't throw
test('doesn\'t throw on valid .env', (t: ExecutionContext) => {
  const env = sPrefix + sToken + sEnv + sEmojiGuild + sFeedbackChannel + sSupers;
  writeFileSync(resolve('.env.test'), env);
  t.notThrows(() => initEnv('.env.test'));
});

// Remove the remaining .env.test file after all tests are done
test.after(() => unlinkSync(resolve('.env.test')));
