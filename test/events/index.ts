import test from 'ava';
import * as events from '../../src/events';
import { isEqual } from '../index.test';

test(
  'events exports everything', isEqual,
  typeof events,
  'object'
);
